# plugin specs
# plugin implementation
from openlog.plugins.extensions.manage_assay.classes import EmptyAssayAdminWizard
from openlog.plugins.hookspecs import ManageAssayHook, hookimpl

#: Name of the plugin that will be referenced in our configuration
PLUGIN_NAME = "manage_assay"


@hookimpl
def manage_assay_plugin():
    return ManageAssayHook(
        name=PLUGIN_NAME,
        wizard=EmptyAssayAdminWizard,
        enable=False,
    )
