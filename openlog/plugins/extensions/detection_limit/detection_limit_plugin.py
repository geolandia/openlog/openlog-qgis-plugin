from openlog.plugins.hookspecs import DetectionLimitHook, hookimpl

#: Name of the plugin that will be referenced in our configuration
PLUGIN_NAME = "detection_limits"


@hookimpl
def detection_plugin():
    return DetectionLimitHook(name=PLUGIN_NAME, enable=False, handler=None)
