from openlog.plugins.connections.geotic.geotic_connection import GeoticConnection
from openlog.plugins.connections.geotic.geotic_connection_dialog import (
    GeoticConnectionDialog,
)
from openlog.plugins.hookspecs import ConnectionHook, hookimpl

#: Name of the plugin that will be referenced in our configuration
PLUGIN_NAME = "Geotic"

# PyQGIS
from qgis.core import QgsApplication
from qgis.PyQt.QtWidgets import QAction


def geotic_action(openlog) -> QAction:
    """
    Function called from plugin manager.
    Args:
        - parent : mainWindow
        - openlog : OpenlogPlugin instance
    """

    def geotic_connect() -> None:
        """
        Execute ConnectionDialog for xplordb database connection

        """
        widget = GeoticConnectionDialog(openlog.iface.mainWindow())
        if widget.exec():
            connection = widget.get_connection_model()
            openlog._define_current_connection(GeoticConnection(connection))

    geotic_connect_action = QAction(
        QgsApplication.getThemeIcon("mIconMssql.svg"),
        PLUGIN_NAME,
        openlog.iface.mainWindow(),
    )
    geotic_connect_action.triggered.connect(geotic_connect)

    return geotic_connect_action


@hookimpl
def connection():
    return ConnectionHook(
        name=PLUGIN_NAME,
        connection_action=geotic_action,
        connection_class=GeoticConnection,
    )
