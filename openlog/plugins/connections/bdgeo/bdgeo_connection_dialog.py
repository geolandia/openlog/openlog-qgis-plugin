from openlog.gui.connection.connection_dialog import ConnectionDialog
from openlog.gui.connection.postgis_connection_widget import PostgisConnectionWidget
from openlog.toolbelt import PlgTranslator


class BDGeoConnectionDialog(ConnectionDialog):
    def __init__(self, parent=None) -> None:
        """
        QDialog to get an bdgeo connection

        Args:
            parent: QWidget
        """
        super(BDGeoConnectionDialog, self).__init__(parent)
        self.set_connection_widget(PostgisConnectionWidget())

        # translation
        self.tr = PlgTranslator().tr
        self.setWindowTitle(self.tr("BDGeo connection"))
