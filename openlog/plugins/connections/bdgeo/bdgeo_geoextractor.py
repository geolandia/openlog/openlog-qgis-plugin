import numpy as np

from openlog.core.geo_extractor import GeoExtractor
from openlog.datamodel.assay.generic_assay import AssayDataExtent


class BDGeoGeoExtractor(GeoExtractor):
    def __init__(self, assay) -> None:
        super().__init__(assay)

    def get_coordinates(self, planned: bool = False):
        if self.assay.assay_definition.data_extent == AssayDataExtent.DISCRETE:
            return self.get_discrete_coordinates()
        else:
            return self.get_extended_coordinates()

    def get_trace_vertices(self, planned: bool = False):
        """
        Return an array describing each vertex coordinates.
        Lines are from first to last vertex, columns ara x,y,z, cumulated length, assay length.
        """
        geom = "trace"
        eoh_col = "total_depth"
        q = f"""
            select st_x(geom) as x, st_y(geom) as y, st_z(geom) as z, eoh
            from (select geom(st_dumppoints(st_transform({geom}, 3857))), {eoh_col} as eoh from qgis.borehole where id = '{self.assay.hole_id}') tmp

            """

        result = self.assay._session.execute(q).fetchall()
        if len(result) == 0:
            return

        arr = np.array(result).astype(float)
        eoh = float(arr[0, 3])
        arr = arr[:, :3]

        # compute cumulated distance
        distance = 0
        dist_list = [0]
        for i in range(arr.shape[0] - 1):
            x1, y1, z1 = arr[i, :]
            x2, y2, z2 = arr[i + 1, :]
            d = np.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2 + (z1 - z2) ** 2)
            distance += d
            dist_list.append(distance)

        # length correction (due to desurveying)
        coef = distance / eoh
        dist_list = np.array(dist_list)
        assay_dist = dist_list / coef
        arr = np.hstack(
            [arr, np.expand_dims(dist_list, axis=1), np.expand_dims(assay_dist, axis=1)]
        )

        return arr

    def get_planned_eoh(self):
        return None

    def get_discrete_coordinates(self) -> np.ndarray:
        geom = "trace"
        eoh_col = "total_depth"

        q = f"""SELECT c_x, c_y, c_z FROM
                (SELECT {self.assay.hole_col.name},o.{self.assay.x_col.name}, {eoh_col},
                st_x(st_transform(ST_3DLineInterpolatePoint(ST_CurveToLine(st_transform({geom}, 3857)), least(o.{self.assay.x_col.name}/{eoh_col}, 1.0)), st_srid({geom}))) AS c_x,
                st_y(st_transform(ST_3DLineInterpolatePoint(ST_CurveToLine(st_transform({geom}, 3857)), least(o.{self.assay.x_col.name}/{eoh_col}, 1.0)), st_srid({geom}))) AS c_y,
                st_z(st_transform(ST_3DLineInterpolatePoint(ST_CurveToLine(st_transform({geom}, 3857)), least(o.{self.assay.x_col.name}/{eoh_col}, 1.0)), st_srid({geom}))) AS c_z
                FROM (SELECT * FROM {self.assay._assay_table.schema}.{self.assay._assay_table.name} where {self.assay.hole_col.name} = '{self.assay.hole_id}') AS o
                LEFT JOIN qgis.borehole AS c
                ON o.{self.assay.hole_col.name} = c.id
                ORDER BY o.{self.assay.x_col.name} ) AS tmp"""
        alt = self.assay._session.execute(q).fetchall()
        if len(alt) == 0:
            return None
        alt = np.array(alt)
        # if geometry doesn't exist
        if alt[0, 0] is None:
            return None
        return alt

    def get_extended_coordinates(self):
        geom = "trace"
        eoh_col = "total_depth"

        q = f"""SELECT f_x, f_y, f_z, t_x, t_y, t_z FROM
                (SELECT {self.assay.hole_col.name},o.{self.assay.x_col.name}, o.{self.assay.x_end_col.name}, {eoh_col},
                st_x(st_transform(ST_3DLineInterpolatePoint(ST_CurveToLine(st_transform({geom}, 3857)), least(o.{self.assay.x_col.name}/{eoh_col}, 1.0)), st_srid({geom}))) AS f_x,
                st_y(st_transform(ST_3DLineInterpolatePoint(ST_CurveToLine(st_transform({geom}, 3857)), least(o.{self.assay.x_col.name}/{eoh_col}, 1.0)), st_srid({geom}))) AS f_y,
                st_z(st_transform(ST_3DLineInterpolatePoint(ST_CurveToLine(st_transform({geom}, 3857)), least(o.{self.assay.x_col.name}/{eoh_col}, 1.0)), st_srid({geom}))) AS f_z,
                st_x(st_transform(ST_3DLineInterpolatePoint(ST_CurveToLine(st_transform({geom}, 3857)), least(o.{self.assay.x_end_col.name}/{eoh_col}, 1.0)), st_srid({geom}))) AS t_x,
                st_y(st_transform(ST_3DLineInterpolatePoint(ST_CurveToLine(st_transform({geom}, 3857)), least(o.{self.assay.x_end_col.name}/{eoh_col}, 1.0)), st_srid({geom}))) AS t_y,
                st_z(st_transform(ST_3DLineInterpolatePoint(ST_CurveToLine(st_transform({geom}, 3857)), least(o.{self.assay.x_end_col.name}/{eoh_col}, 1.0)), st_srid({geom}))) AS t_z
                FROM (SELECT * FROM {self.assay._assay_table.schema}.{self.assay._assay_table.name} where {self.assay.hole_col.name} = '{self.assay.hole_id}') AS o
                LEFT JOIN qgis.borehole AS c
                ON o.{self.assay.hole_col.name} = c.id
                ORDER BY o.{self.assay.x_col.name} ) AS tmp"""

        alt = self.assay._session.execute(q).fetchall()
        if len(alt) == 0:
            return None
        alt = np.array(alt)
        # if geometry doesn't exist
        if alt[0, 0] is None:
            return None
        return alt
