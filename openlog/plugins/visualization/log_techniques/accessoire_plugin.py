# external
import pyqtgraph as pg

# project
from openlog.datamodel.assay.generic_assay import (
    AssayDataExtent,
    AssayDefinition,
    GenericAssay,
)
from openlog.gui.assay_visualization.config.assay_column_visualization_config import (
    AssayColumnVisualizationConfig,
)
from openlog.plugins.hookspecs import SerieTypeHook, hookimpl
from openlog.plugins.visualization.log_techniques.pyqtgraph.tech_log_graphic_item import (
    AccessoireItem,
)

PLUGIN_NAME = "bdgeo_log_technique_accessoire"


# create_assay_column_visualization_config
def create_assay_visualization_column_config(
    extent: AssayDataExtent, assay_definition: AssayDefinition, column_name: str
):
    """Create AssayColumnVisualizationConfig
    Use base config without pen parameter

    Args:
        column (AssayColumn): assay column

    Returns:
        AssayColumnVisualizationConfig: created AssayColumnVisualizationConfig
    """
    column = assay_definition.columns[column_name]
    config = AssayColumnVisualizationConfig(column=column)
    # Remove pen param
    config.pen_params = None
    config.display_plugin_name = PLUGIN_NAME
    return config


# create_plot_items
def create_accessoire(
    plot: pg.PlotWidget,
    assay: GenericAssay,
    column: str,
    config: AssayColumnVisualizationConfig,
) -> None:
    """
    AccessoireItem creation from generic assay

    Args:
        plot (pg.PlotWidget) : plot widget used to add item
        assay (GenericAssay) : assay used for data read
        column (str): assay column used
        config (AssayColumnVisualizationConfig): configuration for display
    """
    data = assay.get_all_values(column)
    item = AccessoireItem(data["accessory"])
    plot.addItem(item)
    config.set_plot_item(item)
    config.set_assay(assay=assay)


@hookimpl
def serie_type():
    return SerieTypeHook(
        name=PLUGIN_NAME,
        create_assay_visualization_column_config=create_assay_visualization_column_config,
        create_plot_item=create_accessoire,
    )
