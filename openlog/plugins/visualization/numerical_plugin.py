# openlog host
import pyqtgraph as pg

from openlog.datamodel.assay.generic_assay import (
    AssayDataExtent,
    AssayDefinition,
    GenericAssay,
)

# plugin
from openlog.plugins.hookspecs import SerieTypeHook, hookimpl
from openlog.plugins.visualization.numerical.config import (
    DiscreteAssayColumnVisualizationConfig,
    ExtendedNumericalAssayColumnVisualizationConfig,
)

#: Name of the plugin that will be referenced in our configuration
PLUGIN_NAME = "numerical"


from openlog.plugins.visualization.numerical.item import (
    create_discrete_plot_item_from_extended_numerical,
    create_discrete_plot_items,
    create_extended_numerical_plot_items,
    create_extended_plot_item_from_discrete_numerical,
)


def create_assay_visualization_column_config(
    extent: AssayDataExtent, assay_definition: AssayDefinition, column_name: str
):

    assay_column = assay_definition.columns[column_name]

    if extent == AssayDataExtent.DISCRETE:
        return DiscreteAssayColumnVisualizationConfig(
            column=assay_column,
            extended_config=ExtendedNumericalAssayColumnVisualizationConfig(
                column=assay_column, discrete_configuration=None
            ),
        )

    else:
        return ExtendedNumericalAssayColumnVisualizationConfig(
            column=assay_column,
            discrete_configuration=DiscreteAssayColumnVisualizationConfig(
                column=assay_column, extended_config=None
            ),
        )


def create_plot_item(plot: pg.PlotWidget, assay: GenericAssay, column: str, config):

    if assay.assay_definition.data_extent == AssayDataExtent.DISCRETE:
        if config.as_extended:
            return create_extended_plot_item_from_discrete_numerical(
                plot, assay, column, config
            )
        else:
            return create_discrete_plot_items(plot, assay, column, config)

    elif assay.assay_definition.data_extent == AssayDataExtent.EXTENDED:
        if config.as_discrete:
            return create_discrete_plot_item_from_extended_numerical(
                plot, assay, column, config
            )
        else:
            return create_extended_numerical_plot_items(plot, assay, column, config)


@hookimpl
def serie_type():
    return SerieTypeHook(
        name=PLUGIN_NAME,
        create_assay_visualization_column_config=create_assay_visualization_column_config,
        create_plot_item=create_plot_item,
    )
