import pyqtgraph as pg

# openlog host
from openlog.datamodel.assay.generic_assay import (
    AssayDataExtent,
    AssayDefinition,
    GenericAssay,
)

# plugin
from openlog.plugins.hookspecs import SerieTypeHook, hookimpl
from openlog.plugins.visualization.imagery.config import (
    ExtendedImageryColumnVisualizationConfig,
)
from openlog.plugins.visualization.imagery.item import (
    create_extended_imagery_plot_items,
)

#: Name of the plugin that will be referenced in our configuration
PLUGIN_NAME = "imagery"


def create_assay_visualization_column_config(
    extent: AssayDataExtent, assay_definition: AssayDefinition, column_name: str
):

    assay_column = assay_definition.columns[column_name]

    if extent == AssayDataExtent.EXTENDED:
        return ExtendedImageryColumnVisualizationConfig(assay_column)
    else:
        return None


def create_plot_item(plot: pg.PlotWidget, assay: GenericAssay, column: str, config):

    return create_extended_imagery_plot_items(plot, assay, column, config)


@hookimpl
def serie_type():
    return SerieTypeHook(
        name=PLUGIN_NAME,
        create_assay_visualization_column_config=create_assay_visualization_column_config,
        create_plot_item=create_plot_item,
    )
