import pyqtgraph as pg

from openlog.datamodel.assay.generic_assay import GenericAssay
from openlog.gui.assay_visualization.config.assay_column_visualization_config import (
    AssayColumnVisualizationConfig,
)
from openlog.gui.assay_visualization.extended.text_bar_graph_items import (
    TextBarGraphItems,
)
from openlog.gui.assay_visualization.plot_item_utils import (
    disable_tick_and_value_display,
)


def create_extended_nominal_plot_items(
    plot: pg.PlotWidget,
    assay: GenericAssay,
    column: str,
    config: AssayColumnVisualizationConfig,
) -> None:
    # For now width is hard coded to 10.0
    item = TextBarGraphItems(
        assay, column, 0, 10.0, altitude=assay.get_altitude(), is_categorical=False
    )
    item.add_to_plot(plot)
    config.set_plot_item(item)

    # Disable auto range because of pyqtgraph issue :
    # If there is not enough space do display text, pyqtgraph has an infinite loop for auto range calculation
    plot.getViewBox().enableAutoRange(x=False)

    plot.setXRange(0, 10.0)
    disable_tick_and_value_display(plot, "top")
    disable_tick_and_value_display(plot, "bottom")
