# openlog host
import pyqtgraph as pg

from openlog.datamodel.assay.generic_assay import (
    AssayDataExtent,
    AssayDefinition,
    GenericAssay,
)

# plugin specs
from openlog.plugins.hookspecs import SerieTypeHook, hookimpl

# plugin implementation
from openlog.plugins.visualization.categorical.config import (
    DiscreteCategoricalAssayColumnVisualizationConfig,
    ExtendedCategoricalAssayColumnVisualizationConfig,
)
from openlog.plugins.visualization.categorical.item import (
    create_discrete_categorical_plot_items,
    create_extended_categorical_plot_items,
)

#: Name of the plugin that will be referenced in our configuration
PLUGIN_NAME = "categorical"


def create_assay_visualization_column_config(
    extent: AssayDataExtent, assay_definition: AssayDefinition, column_name: str
):

    assay_column = assay_definition.columns[column_name]

    if extent == AssayDataExtent.DISCRETE:
        return DiscreteCategoricalAssayColumnVisualizationConfig(column=assay_column)

    else:
        return ExtendedCategoricalAssayColumnVisualizationConfig(column=assay_column)


def create_plot_item(plot: pg.PlotWidget, assay: GenericAssay, column: str, config):

    if assay.assay_definition.data_extent == AssayDataExtent.DISCRETE:
        return create_discrete_categorical_plot_items(plot, assay, column, config)
    else:
        return create_extended_categorical_plot_items(plot, assay, column, config)


@hookimpl
def serie_type():
    return SerieTypeHook(
        name=PLUGIN_NAME,
        create_assay_visualization_column_config=create_assay_visualization_column_config,
        create_plot_item=create_plot_item,
    )
