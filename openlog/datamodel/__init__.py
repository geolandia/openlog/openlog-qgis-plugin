"""
This package contains modules to access all data from an openlog connection.


- ``openlog.datamodel.assay`` : contains all classes used to define an assay
- ``openlog.datamodel.connection`` : contains all classes used to define an openlog connection

Several interfaces are available to acces data from an openlog connection :

=============================    =============================================================================
Interface                        Description
=============================    =============================================================================
:class:`.AssayInterface`         import or read assays from openlog connection
:class:`.CategoriesInterface`    import or read categories from openlog connection
:class:`.LayersInterface`        access QGIS layer for openlog connection
:class:`.ReadInterface`          read data from openlog connection (person, dataset, collar, survey, lithology)
:class:`.WriteInterface`         write data into openlog connection (person, dataset, collar, survey, lithology)
=============================    =============================================================================

"""
