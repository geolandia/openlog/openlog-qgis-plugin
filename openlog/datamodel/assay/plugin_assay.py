from openlog.datamodel.assay.generic_assay import AssayDefinition, GenericAssay


class PluginColumnAssay(GenericAssay):
    def __init__(
        self,
        hole_id: str,
        assay_definition: AssayDefinition,
        **kwargs,
    ):
        """GenericAssay only for data read that use plugin to read column data

        Args:
            hole_id (str): collar hole_id
            assay_definition (AssayDefinition): definition of assay (variable, domain, ...)
            **kwargs: Arbitrary keyword arguments. Define option for plugin use
        """
        super().__init__(
            hole_id=hole_id,
            assay_definition=assay_definition,
            use_assay_column_plugin_reader=True,
        )
        self.kwargs = kwargs
