from dataclasses import dataclass


@dataclass
class CategoriesTableDefinition:
    name: str = ""
    table_name: str = ""
    name_column: str = "name"
    schema: str = ""
