from openlog.core.geo_extractor import SpatialiteGeoExtractor
from openlog.core.trace_splitter import SpatialiteSplitTracesQueries
from openlog.datamodel.assay.generic_assay import (
    AssayColumn,
    AssayDatabaseDefinition,
    AssayDataExtent,
    AssayDefinition,
    AssayDomainType,
    AssaySeriesType,
    GenericAssay,
)
from openlog.datamodel.connection.interfaces.categories_interface import (
    CategoriesInterface,
)
from openlog.datamodel.connection.sqlalchemy.sqlalchemy_assay_interface import (
    SqlAlchemyAssayInterface,
)


class SpatialiteAssayInterface(SqlAlchemyAssayInterface):
    def __init__(self, engine, session, categories_iface: CategoriesInterface):
        """
        Implement AssayInterface for a Spatialite db

        Args:
            engine: sqlalchemy engine
            session: sqlalchemy session created from engine
            categories_iface : CategoriesInterface to get default lith category name
        """

        self._categories_iface = categories_iface
        super().__init__(engine, session)
        self.trace_splitter = SpatialiteSplitTracesQueries
        self.geo_extractor = SpatialiteGeoExtractor

    def delete_assay_from_database(self, variable: str, only_splitted: bool = False):

        # get splitted layers
        q = f'SELECT name FROM assay_column WHERE assay = "{variable}" AND series_type IN ("numerical", "categorical");'
        splittable_columns = self.session.execute(q).fetchall()
        splittable_columns = [elt[0] for elt in splittable_columns]

        splitted_layers = []
        trigger_names = []
        for column in splittable_columns:
            for geom in ["trace", "planned_trace"]:
                l = "_".join([variable, column, geom])
                splitted_layers.append(l)
                for op in ["update", "insert", "delete"]:
                    for table in ["collar", "assay"]:
                        s = "_".join([op, table, variable, column, geom])
                        trigger_names.append(s)

        # delete triggers
        for trigger in trigger_names:
            q = f'DROP TRIGGER IF EXISTS "{trigger}";'
            self.session.execute(q)
            self.session.commit()

        # delete trace layers
        for layer in splitted_layers:
            q = f"SELECT DropTable(NULL, '{layer}', 1) ;"
            self.session.execute(q)
            self.session.commit()

        if only_splitted:
            return

        # delete assay table
        q = f"DROP TABLE IF EXISTS {variable};"
        self.session.execute(q)
        self.session.commit()
        # delete assay definitions
        q = f'DELETE FROM assay_column WHERE assay = "{variable}";'
        self.session.execute(q)
        self.session.commit()

        q = f'DELETE FROM assay_column_definition WHERE variable = "{variable}";'
        self.session.execute(q)
        self.session.commit()

        q = f'DELETE FROM assay_definition WHERE table_name = "{variable}";'
        self.session.execute(q)
        self.session.commit()

        q = f'DELETE FROM assays WHERE variable = "{variable}";'
        self.session.execute(q)
        self.session.commit()

    def can_administrate_assays(self) -> dict:

        return {"creation": True, "deletion": True}

    def can_save_symbology_in_db(self) -> bool:
        """
        Check is database can store assay symbology.
        """

        return True
