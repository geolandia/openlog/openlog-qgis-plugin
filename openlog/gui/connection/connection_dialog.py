import os

from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QDialog, QMessageBox

from openlog.datamodel.connection.openlog_connection import Connection
from openlog.gui.connection.connection_widget import ConnectionWidget
from openlog.toolbelt import PlgTranslator


class ConnectionDialog(QDialog):
    def __init__(self, parent=None) -> None:
        """
        QDialog to get openlog connection

        Args:
            parent: QWidget
        """
        super().__init__(parent)
        uic.loadUi(
            os.path.join(os.path.dirname(__file__), "connection_dialog.ui"), self
        )

        # translation
        self.tr = PlgTranslator().tr
        self.setWindowTitle(self.tr("OpenLog connection"))

        self.connection_widget = None

    def is_valid(self) -> bool:
        """
        Check if connection is valid byt testing connection widget if defined

        Returns: True if connection is valid

        """
        if self.connection_widget:
            return self.connection_widget.test_connection()
        else:
            return False

    def accept(self) -> None:
        """
        Check if connection is valid and close dialog

        """
        # We need to use context for translation because some classes inherits ConnectionDialog
        if self.is_valid():
            selected_model = self.connection_widget.get_connection_model()
            saved_model = self.connection_widget.get_connection_from_settings(
                selected_model.name
            )

            if not selected_model.name:
                ret = QMessageBox.question(
                    self,
                    self.tr("Save connection", "ConnectionDialog"),
                    self.tr(
                        "Do you want to save current connection parameters ?",
                        "ConnectionDialog",
                    ),
                )
                if ret == QMessageBox.Yes:
                    self.connection_widget.add_connection()
            elif selected_model != saved_model:
                ret = QMessageBox.question(
                    self,
                    self.tr("Save connection", "ConnectionDialog"),
                    self.tr(
                        "Connection parameter changed, save changes ?",
                        "ConnectionDialog",
                    ),
                )
                if ret == QMessageBox.Yes:
                    self.connection_widget.save_current_connection()

            super(ConnectionDialog, self).accept()

    def set_connection_widget(self, connection_widget: ConnectionWidget):
        """
        Define dialog used connection widget

        Args:
            connection_widget: connection widget
        """
        self.connection_widget = connection_widget
        self.verticalLayout.addWidget(self.connection_widget)

    def set_connection_model(self, connection: Connection) -> None:
        """
        Define current widget parameters from openlog connection

        Args:
            connection: openlog connection
        """
        self.connection_widget.set_connection_model(connection)

    def get_connection_model(self) -> Connection:
        """
        Return openlog connection from widget

        Returns: openlog connnection

        """
        return self.connection_widget.get_connection_model()
