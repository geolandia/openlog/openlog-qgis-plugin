from PyQt5.QtWidgets import QWizard

from openlog.gui.create_database.wizard.connection import ConnectionPageWizard
from openlog.gui.create_database.wizard.database_creation import DatabaseCreationPageWizard
from openlog.toolbelt import PlgTranslator


class DatabaseCreationWizard(QWizard):

    def __init__(self, parent=None) -> None:
        """
        QWizard to create xplordb database.

        """
        super().__init__(parent)

        # translation
        self.tr = PlgTranslator().tr
        self.setWindowTitle(self.tr("Xplordb database creation"))

        # Connection wizard used to store database connection
        self.connection_page = ConnectionPageWizard()
        self.addPage(self.connection_page)

        self.database_creation = DatabaseCreationPageWizard(self.connection_page)
        self.addPage(self.database_creation)
