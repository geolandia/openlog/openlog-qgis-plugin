import pyqtgraph as pg

from openlog.datamodel.assay.generic_assay import GenericAssay
from openlog.gui.assay_visualization.config.assay_column_visualization_config import (
    AssayColumnVisualizationConfig,
)
from openlog.plugins.manager import get_plugin_manager


class AssayPlotItemFactory:
    """Factory class for assay column plot item creation"""

    def create_plot_items(
        self,
        plot: pg.PlotWidget,
        assay: GenericAssay,
        column: str,
        config: AssayColumnVisualizationConfig,
    ) -> None:
        """
        Create an pg.PlotDataItem creation from generic assay

        Args:
            plot: pg.PlotWidget
            assay : GenericAssay
            column: assay column
            config:

        Returns: (pg.PlotDataItem)

        """
        manager = get_plugin_manager()

        if config.display_plugin_name is not None:
            hook = manager.get_one_serie_type(name=config.display_plugin_name)
        else:
            serie_type = assay.assay_definition.columns[column].series_type.value
            hook = manager.get_one_serie_type(name=serie_type)
        hook.create_plot_item(plot, assay, column, config)

        config.set_assay(assay)
