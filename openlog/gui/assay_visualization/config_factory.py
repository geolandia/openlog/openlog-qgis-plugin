from openlog.datamodel.assay.generic_assay import AssayDefinition
from openlog.datamodel.connection.interfaces.assay_interface import AssayInterface
from openlog.gui.assay_visualization.config.assay_column_visualization_config import (
    AssayColumnVisualizationConfig,
)

# plugins
from openlog.plugins.manager import get_plugin_manager

manager = get_plugin_manager()


class AssayColumnVisualizationConfigFactory:
    """Factory class for assay column visualization creation"""

    def create_config(
        self,
        assay_definition: AssayDefinition,
        column: str,
        assay_iface: AssayInterface,
    ) -> AssayColumnVisualizationConfig:
        """
        Create an assay column visualization config from assay definition

        Args:
            assay_definition: AssayDefinition
            column: assay column

        Returns: (AssayColumnVisualizationConfig)

        """
        extent = assay_definition.data_extent

        # Use plugin for configuration creation if defined in AssayColumn
        assay_column = assay_definition.columns[column]
        if assay_column.display_plugin_name is not None:
            config = manager.get_one_serie_type(
                name=assay_column.display_plugin_name
            ).create_assay_visualization_column_config(extent, assay_definition, column)
            config.set_assay_iface(assay_iface)
            return config

        serie_type = assay_definition.columns[column].series_type.value
        config = manager.get_one_serie_type(
            name=serie_type
        ).create_assay_visualization_column_config(extent, assay_definition, column)
        config.set_assay_iface(assay_iface)
        return config
