from qgis.PyQt.QtGui import QColor


def serialize_pen_param(config, param_name: str, key_prefix: str) -> dict:

    d = {}
    # get PenParameter instance
    parameter = getattr(config, param_name)
    qcolor = parameter.pen.color()
    d[key_prefix + "_color"] = qcolor.name()
    d[key_prefix + "_width"] = parameter.pen.width()
    d[key_prefix + "_style"] = parameter.pen.style()
    d[key_prefix + "_capstyle"] = parameter.pen.capStyle()
    d[key_prefix + "_joinstyle"] = parameter.pen.joinStyle()
    d[key_prefix + "_cosmetic"] = parameter.pen.isCosmetic()

    return d


def deserialize_pen_param(config, dict, param_name: str, key_prefix: str) -> None:

    parameter = getattr(config, param_name)

    parameter.blockSignals(True)
    pen = parameter.pen
    qcolor = QColor()
    qcolor.setNamedColor(dict.get(key_prefix + "_color"))
    pen.setColor(qcolor)
    pen.setWidth(dict.get(key_prefix + "_width"))
    pen.setStyle(dict.get(key_prefix + "_style"))
    pen.setCapStyle(dict.get(key_prefix + "_capstyle"))
    pen.setJoinStyle(dict.get(key_prefix + "_joinstyle"))
    pen.setCosmetic(dict.get(key_prefix + "_cosmetic"))
    # update parameters according to QPen
    parameter.updateFromPen(parameter, pen)
    parameter.blockSignals(False)
