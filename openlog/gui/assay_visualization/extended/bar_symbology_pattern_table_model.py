from qgis.PyQt import QtCore
from qgis.PyQt.QtCore import QVariant
from qgis.PyQt.QtGui import QIcon

from openlog.gui.assay_visualization.extended.bar_symbology import BarSymbology
from openlog.toolbelt.vertical_string_map_table_model import VerticalStringMapTableModel


class BarSymbologyPatternTableModel(VerticalStringMapTableModel):
    def __init__(self, parent=None) -> None:
        """
        QStandardItemModel for BarSymbology pattern dict display

        Args:
            parent: QWidget
        """
        super().__init__(parent=parent)
        key_label = self.tr("Key")
        val_label = self.tr("Pattern")

        self.setHorizontalHeaderLabels([key_label, val_label])

    def data(
        self, index: QtCore.QModelIndex, role: int = QtCore.Qt.DisplayRole
    ) -> QVariant:
        """
        Override VerticalStringMapTableModel data() for :
        - icon of pattern col

        Args:
            index: QModelIndex
            role: Qt role

        Returns: QVariant

        """
        result = super().data(index, role)

        if role == QtCore.Qt.DecorationRole and index.column() == self.VALUE_COL:
            pattern = str(self.data(index, QtCore.Qt.DisplayRole))
            pattern = BarSymbology().replace_alias_in_pattern(pattern)
            result = QIcon(pattern)

        return result
