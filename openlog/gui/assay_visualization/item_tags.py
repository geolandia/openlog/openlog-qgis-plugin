"""
Empty classes used to mark item classes.
"""


class LegendTag:
    """
    is a legendable item ?

    """

    pass


class CategoricalLegendTag(LegendTag):
    """
    A legend item by category (multiple legend item for one assay)
    """

    pass


class SimpleLegendTag(LegendTag):
    """
    A legend item by assay

    """

    pass


class InspectorTag:
    """
    is this item scanned by inspector line ?
    Should implement get_intersection_points(inspector: InspectorLine) method in subclass.
    """

    def get_intersection_points():

        return
