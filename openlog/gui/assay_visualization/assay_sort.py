from dataclasses import dataclass
from enum import Enum


class AssaySort(Enum):
    @dataclass
    class Azimuth:
        value: float

        def set_value(self, value):
            self.value = value

    ASSAY_NAME = "assay_name"
    HOLE_ID = "hole_id"
    X = "x"
    Y = "y"
    UNDEFINED = "undefined"
    AZIMUTH = Azimuth(0.0)

    @classmethod
    def set_azimuth(cls, value: float):
        cls.AZIMUTH.value.set_value(value)
        return cls
