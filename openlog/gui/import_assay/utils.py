import re


def sanitize_sql_name(name):
    """
    Rend une chaîne de caractères compatible pour un nom de table PostgreSQL.

    :param name: La chaîne à transformer
    :return: Une version nettoyée et compatible pour PostgreSQL
    """
    sanitized = name.lower()
    sanitized = re.sub(r"[ -]+", "_", sanitized)
    sanitized = re.sub(r"[^a-z0-9_]", "", sanitized)

    if sanitized == "":
        return sanitized

    if not sanitized[0].isalpha():
        sanitized = f"table_{sanitized}"
    return sanitized
