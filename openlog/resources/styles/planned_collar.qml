<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale="0" autoRefreshMode="Disabled" autoRefreshTime="0" minScale="100000000" labelsEnabled="0" simplifyMaxScale="1" simplifyDrawingHints="0" hasScaleBasedVisibilityFlag="0" symbologyReferenceScale="-1" simplifyAlgorithm="0" simplifyDrawingTol="1" readOnly="0" simplifyLocal="1" version="3.40.0-Bratislava" styleCategories="AllStyleCategories">
  <renderer-3d type="vector" layer="Collar_____home_vincent_test_old_db__892d7250_ee19_4c88_bd34_fcfe90715bda">
    <vector-layer-3d-tiling show-bounding-boxes="0" zoom-levels-count="3"/>
    <symbol type="point" shape="sphere" material_type="phong">
      <data alt-clamping="absolute"/>
      <material kd="1" ka="1" shininess="0" opacity="1" ambient="26,26,26,255,rgb:0.10196078431372549,0.10196078431372549,0.10196078431372549,1" diffuse="178,178,178,255,rgb:0.69803921568627447,0.69803921568627447,0.69803921568627447,1" ks="1" specular="255,255,255,255,rgb:1,1,1,1">
        <data-defined-properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data-defined-properties>
      </material>
      <shape-properties>
        <Option type="Map">
          <Option value="" type="QString" name="model"/>
          <Option value="10" type="double" name="radius"/>
        </Option>
      </shape-properties>
      <transform matrix="1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1"/>
      <symbol type="marker" name="symbol" force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer id="{312b8322-66e4-4cb1-87cf-a6c795d19787}" class="SimpleMarker" enabled="1" pass="0" locked="0">
          <Option type="Map">
            <Option value="0" type="QString" name="angle"/>
            <Option value="square" type="QString" name="cap_style"/>
            <Option value="125,139,143,255,rgb:0.49019607843137253,0.54509803921568623,0.5607843137254902,1" type="QString" name="color"/>
            <Option value="1" type="QString" name="horizontal_anchor_point"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="circle" type="QString" name="name"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0" type="QString" name="outline_width"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="outline_width_map_unit_scale"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="diameter" type="QString" name="scale_method"/>
            <Option value="2" type="QString" name="size"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="size_map_unit_scale"/>
            <Option value="MM" type="QString" name="size_unit"/>
            <Option value="1" type="QString" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbol>
  </renderer-3d>
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal endField="" durationUnit="min" enabled="0" fixedDuration="0" durationField="x" startExpression="" endExpression="" mode="0" limitMode="0" startField="load_date" accumulate="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation zscale="1" type="IndividualFeatures" clamping="Absolute" showMarkerSymbolInSurfacePlots="0" extrusionEnabled="0" symbology="Line" extrusion="0" zoffset="0" respectLayerSymbol="1" binding="Centroid">
    <data-defined-properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol type="line" name="" force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer id="{78ee6cc1-88d4-4ca1-865b-9e73dd86ac84}" class="SimpleLine" enabled="1" pass="0" locked="0">
          <Option type="Map">
            <Option value="0" type="QString" name="align_dash_pattern"/>
            <Option value="square" type="QString" name="capstyle"/>
            <Option value="5;2" type="QString" name="customdash"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="customdash_map_unit_scale"/>
            <Option value="MM" type="QString" name="customdash_unit"/>
            <Option value="0" type="QString" name="dash_pattern_offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="dash_pattern_offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="dash_pattern_offset_unit"/>
            <Option value="0" type="QString" name="draw_inside_polygon"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="213,180,60,255,rgb:0.83529411764705885,0.70588235294117652,0.23529411764705882,1" type="QString" name="line_color"/>
            <Option value="solid" type="QString" name="line_style"/>
            <Option value="0.6" type="QString" name="line_width"/>
            <Option value="MM" type="QString" name="line_width_unit"/>
            <Option value="0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="0" type="QString" name="ring_filter"/>
            <Option value="0" type="QString" name="trim_distance_end"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_end_map_unit_scale"/>
            <Option value="MM" type="QString" name="trim_distance_end_unit"/>
            <Option value="0" type="QString" name="trim_distance_start"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_start_map_unit_scale"/>
            <Option value="MM" type="QString" name="trim_distance_start_unit"/>
            <Option value="0" type="QString" name="tweak_dash_pattern_on_corners"/>
            <Option value="0" type="QString" name="use_custom_dash"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="width_map_unit_scale"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol type="fill" name="" force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer id="{17e3beb9-ec58-432e-ae6d-43e08e5657ab}" class="SimpleFill" enabled="1" pass="0" locked="0">
          <Option type="Map">
            <Option value="3x:0,0,0,0,0,0" type="QString" name="border_width_map_unit_scale"/>
            <Option value="213,180,60,255,rgb:0.83529411764705885,0.70588235294117652,0.23529411764705882,1" type="QString" name="color"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="152,129,43,255,rgb:0.59662775616083008,0.50420386053253985,0.1680628671702144,1" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0.2" type="QString" name="outline_width"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="solid" type="QString" name="style"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol type="marker" name="" force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer id="{c0a24ea1-e8e0-4270-a494-d5b3c60576e6}" class="SimpleMarker" enabled="1" pass="0" locked="0">
          <Option type="Map">
            <Option value="0" type="QString" name="angle"/>
            <Option value="square" type="QString" name="cap_style"/>
            <Option value="213,180,60,255,rgb:0.83529411764705885,0.70588235294117652,0.23529411764705882,1" type="QString" name="color"/>
            <Option value="1" type="QString" name="horizontal_anchor_point"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="diamond" type="QString" name="name"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="152,129,43,255,rgb:0.59662775616083008,0.50420386053253985,0.1680628671702144,1" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0.2" type="QString" name="outline_width"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="outline_width_map_unit_scale"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="diameter" type="QString" name="scale_method"/>
            <Option value="3" type="QString" name="size"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="size_map_unit_scale"/>
            <Option value="MM" type="QString" name="size_unit"/>
            <Option value="1" type="QString" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 symbollevels="0" type="singleSymbol" referencescale="-1" forceraster="0" enableorderby="0">
    <symbols>
      <symbol type="marker" name="0" force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer id="{edd8d8d8-f116-4710-b2d8-c788690c92f3}" class="SimpleMarker" enabled="1" pass="0" locked="0">
          <Option type="Map">
            <Option value="0" type="QString" name="angle"/>
            <Option value="square" type="QString" name="cap_style"/>
            <Option value="31,120,180,255,rgb:0.12156862745098039,0.47058823529411764,0.70588235294117652,1" type="QString" name="color"/>
            <Option value="1" type="QString" name="horizontal_anchor_point"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="square" type="QString" name="name"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="2,0,1,255,hsv:0.95277777777777772,0.98431372549019602,0.00784313725490196,1" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0" type="QString" name="outline_width"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="outline_width_map_unit_scale"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="area" type="QString" name="scale_method"/>
            <Option value="2" type="QString" name="size"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="size_map_unit_scale"/>
            <Option value="MM" type="QString" name="size_unit"/>
            <Option value="1" type="QString" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
    <data-defined-properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </data-defined-properties>
  </renderer-v2>
  <selection mode="Default">
    <selectionColor invalid="1"/>
    <selectionSymbol>
      <symbol type="marker" name="" force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer id="{c3ac81d0-765f-442c-84ae-a1c56d9409dc}" class="SimpleMarker" enabled="1" pass="0" locked="0">
          <Option type="Map">
            <Option value="0" type="QString" name="angle"/>
            <Option value="square" type="QString" name="cap_style"/>
            <Option value="255,0,0,255,rgb:1,0,0,1" type="QString" name="color"/>
            <Option value="1" type="QString" name="horizontal_anchor_point"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="circle" type="QString" name="name"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0" type="QString" name="outline_width"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="outline_width_map_unit_scale"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="diameter" type="QString" name="scale_method"/>
            <Option value="2" type="QString" name="size"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="size_map_unit_scale"/>
            <Option value="MM" type="QString" name="size_unit"/>
            <Option value="1" type="QString" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </selectionSymbol>
  </selection>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style fontKerning="1" fontStrikeout="0" textOrientation="horizontal" multilineHeightUnit="Percentage" fontWeight="75" legendString="Aa" fontItalic="1" fontSizeMapUnitScale="3x:0,0,0,0,0,0" allowHtml="0" previewBkgrdColor="255,255,255,255,rgb:1,1,1,1" textOpacity="1" isExpression="0" fontSizeUnit="Point" fieldName="hole_id" useSubstitutions="0" textColor="0,0,0,255,rgb:0,0,0,1" fontLetterSpacing="0" forcedBold="0" blendMode="0" tabStopDistanceUnit="Point" tabStopDistance="80" capitalization="0" forcedItalic="0" fontWordSpacing="0" multilineHeight="1" fontFamily="Noto Sans" fontUnderline="0" tabStopDistanceMapUnitScale="3x:0,0,0,0,0,0" fontSize="9" namedStyle="Bold Italic">
        <families/>
        <text-buffer bufferColor="255,255,255,255,rgb:1,1,1,1" bufferOpacity="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferDraw="0" bufferNoFill="0" bufferSize="1" bufferSizeUnits="MM" bufferJoinStyle="128" bufferBlendMode="0"/>
        <text-mask maskEnabled="0" maskedSymbolLayers="" maskJoinStyle="128" maskType="0" maskSize="1.5" maskOpacity="1" maskSizeUnits="MM" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskSize2="1.5"/>
        <background shapeOpacity="1" shapeSizeX="0" shapeRadiiUnit="MM" shapeJoinStyle="64" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeUnit="MM" shapeType="0" shapeRotation="0" shapeSizeY="0" shapeRadiiY="0" shapeBorderColor="128,128,128,255,rgb:0.50196078431372548,0.50196078431372548,0.50196078431372548,1" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeFillColor="255,255,255,255,rgb:1,1,1,1" shapeDraw="0" shapeSVGFile="" shapeBorderWidthUnit="MM" shapeOffsetUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRotationType="0" shapeRadiiX="0" shapeBlendMode="0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBorderWidth="0" shapeSizeType="0" shapeOffsetX="0" shapeOffsetY="0">
          <symbol type="marker" name="markerSymbol" force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" alpha="1">
            <data_defined_properties>
              <Option type="Map">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
            </data_defined_properties>
            <layer id="" class="SimpleMarker" enabled="1" pass="0" locked="0">
              <Option type="Map">
                <Option value="0" type="QString" name="angle"/>
                <Option value="square" type="QString" name="cap_style"/>
                <Option value="164,113,88,255,rgb:0.64313725490196083,0.44313725490196076,0.34509803921568627,1" type="QString" name="color"/>
                <Option value="1" type="QString" name="horizontal_anchor_point"/>
                <Option value="bevel" type="QString" name="joinstyle"/>
                <Option value="circle" type="QString" name="name"/>
                <Option value="0,0" type="QString" name="offset"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
                <Option value="MM" type="QString" name="offset_unit"/>
                <Option value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" type="QString" name="outline_color"/>
                <Option value="solid" type="QString" name="outline_style"/>
                <Option value="0" type="QString" name="outline_width"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="outline_width_map_unit_scale"/>
                <Option value="MM" type="QString" name="outline_width_unit"/>
                <Option value="diameter" type="QString" name="scale_method"/>
                <Option value="2" type="QString" name="size"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="size_map_unit_scale"/>
                <Option value="MM" type="QString" name="size_unit"/>
                <Option value="1" type="QString" name="vertical_anchor_point"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" type="QString" name="name"/>
                  <Option name="properties"/>
                  <Option value="collection" type="QString" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
          <symbol type="fill" name="fillSymbol" force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" alpha="1">
            <data_defined_properties>
              <Option type="Map">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
            </data_defined_properties>
            <layer id="" class="SimpleFill" enabled="1" pass="0" locked="0">
              <Option type="Map">
                <Option value="3x:0,0,0,0,0,0" type="QString" name="border_width_map_unit_scale"/>
                <Option value="255,255,255,255,rgb:1,1,1,1" type="QString" name="color"/>
                <Option value="bevel" type="QString" name="joinstyle"/>
                <Option value="0,0" type="QString" name="offset"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
                <Option value="MM" type="QString" name="offset_unit"/>
                <Option value="128,128,128,255,rgb:0.50196078431372548,0.50196078431372548,0.50196078431372548,1" type="QString" name="outline_color"/>
                <Option value="no" type="QString" name="outline_style"/>
                <Option value="0" type="QString" name="outline_width"/>
                <Option value="Point" type="QString" name="outline_width_unit"/>
                <Option value="solid" type="QString" name="style"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" type="QString" name="name"/>
                  <Option name="properties"/>
                  <Option value="collection" type="QString" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowOffsetAngle="135" shadowOffsetGlobal="1" shadowOffsetUnit="MM" shadowDraw="0" shadowOffsetDist="1" shadowUnder="0" shadowRadius="1.5" shadowOpacity="0.69999999999999996" shadowRadiusUnit="MM" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowScale="100" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowColor="0,0,0,255,rgb:0,0,0,1" shadowRadiusAlphaOnly="0" shadowBlendMode="6"/>
        <dd_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format plussign="0" formatNumbers="0" multilineAlign="3" wrapChar="" useMaxLineLengthForAutoWrap="1" autoWrapLength="0" leftDirectionSymbol="&lt;" placeDirectionSymbol="0" addDirectionSymbol="0" rightDirectionSymbol=">" reverseDirectionSymbol="0" decimals="3"/>
      <placement maxCurvedCharAngleIn="25" geometryGeneratorEnabled="0" preserveRotation="1" lineAnchorClipping="0" polygonPlacementFlags="2" priority="5" layerType="PointGeometry" overrunDistanceUnit="MM" dist="2" rotationAngle="0" allowDegraded="0" fitInPolygonOnly="0" distMapUnitScale="3x:0,0,0,0,0,0" placementFlags="10" offsetType="0" centroidWhole="0" prioritization="PreferCloser" lineAnchorPercent="0.5" maximumDistance="0" placement="6" rotationUnit="AngleDegrees" maximumDistanceMapUnitScale="3x:0,0,0,0,0,0" repeatDistanceUnits="MM" overlapHandling="PreventOverlap" centroidInside="0" lineAnchorTextPoint="CenterOfText" quadOffset="4" maximumDistanceUnit="MM" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGenerator="" lineAnchorType="0" xOffset="0" yOffset="0" distUnits="MM" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="PointGeometry" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" repeatDistance="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" overrunDistance="0" maxCurvedCharAngleOut="-25" offsetUnits="MapUnit"/>
      <rendering drawLabels="1" fontMinPixelSize="3" obstacleType="0" zIndex="0" labelPerPart="0" scaleMax="10000000" limitNumLabels="0" fontMaxPixelSize="10000" scaleMin="1" mergeLines="0" obstacleFactor="1" fontLimitPixelSize="0" minFeatureSize="0" upsidedownLabels="0" unplacedVisibility="0" maxNumLabels="2000" obstacle="1" scaleVisibility="0"/>
      <dd_properties>
        <Option type="Map">
          <Option value="" type="QString" name="name"/>
          <Option name="properties"/>
          <Option value="collection" type="QString" name="type"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option value="pole_of_inaccessibility" type="QString" name="anchorPoint"/>
          <Option value="0" type="int" name="blendMode"/>
          <Option type="Map" name="ddProperties">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
          <Option value="false" type="bool" name="drawToAllParts"/>
          <Option value="0" type="QString" name="enabled"/>
          <Option value="point_on_exterior" type="QString" name="labelAnchorPoint"/>
          <Option value="&lt;symbol type=&quot;line&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; frame_rate=&quot;10&quot; is_animated=&quot;0&quot; clip_to_extent=&quot;1&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; type=&quot;QString&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; type=&quot;QString&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer id=&quot;{a8d52701-88d4-4f30-930f-2780be073486}&quot; class=&quot;SimpleLine&quot; enabled=&quot;1&quot; pass=&quot;0&quot; locked=&quot;0&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;0&quot; type=&quot;QString&quot; name=&quot;align_dash_pattern&quot;/>&lt;Option value=&quot;square&quot; type=&quot;QString&quot; name=&quot;capstyle&quot;/>&lt;Option value=&quot;5;2&quot; type=&quot;QString&quot; name=&quot;customdash&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot; name=&quot;customdash_map_unit_scale&quot;/>&lt;Option value=&quot;MM&quot; type=&quot;QString&quot; name=&quot;customdash_unit&quot;/>&lt;Option value=&quot;0&quot; type=&quot;QString&quot; name=&quot;dash_pattern_offset&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot; name=&quot;dash_pattern_offset_map_unit_scale&quot;/>&lt;Option value=&quot;MM&quot; type=&quot;QString&quot; name=&quot;dash_pattern_offset_unit&quot;/>&lt;Option value=&quot;0&quot; type=&quot;QString&quot; name=&quot;draw_inside_polygon&quot;/>&lt;Option value=&quot;bevel&quot; type=&quot;QString&quot; name=&quot;joinstyle&quot;/>&lt;Option value=&quot;60,60,60,255,rgb:0.23529411764705882,0.23529411764705882,0.23529411764705882,1&quot; type=&quot;QString&quot; name=&quot;line_color&quot;/>&lt;Option value=&quot;solid&quot; type=&quot;QString&quot; name=&quot;line_style&quot;/>&lt;Option value=&quot;0.3&quot; type=&quot;QString&quot; name=&quot;line_width&quot;/>&lt;Option value=&quot;MM&quot; type=&quot;QString&quot; name=&quot;line_width_unit&quot;/>&lt;Option value=&quot;0&quot; type=&quot;QString&quot; name=&quot;offset&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot;/>&lt;Option value=&quot;MM&quot; type=&quot;QString&quot; name=&quot;offset_unit&quot;/>&lt;Option value=&quot;0&quot; type=&quot;QString&quot; name=&quot;ring_filter&quot;/>&lt;Option value=&quot;0&quot; type=&quot;QString&quot; name=&quot;trim_distance_end&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot; name=&quot;trim_distance_end_map_unit_scale&quot;/>&lt;Option value=&quot;MM&quot; type=&quot;QString&quot; name=&quot;trim_distance_end_unit&quot;/>&lt;Option value=&quot;0&quot; type=&quot;QString&quot; name=&quot;trim_distance_start&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot; name=&quot;trim_distance_start_map_unit_scale&quot;/>&lt;Option value=&quot;MM&quot; type=&quot;QString&quot; name=&quot;trim_distance_start_unit&quot;/>&lt;Option value=&quot;0&quot; type=&quot;QString&quot; name=&quot;tweak_dash_pattern_on_corners&quot;/>&lt;Option value=&quot;0&quot; type=&quot;QString&quot; name=&quot;use_custom_dash&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; type=&quot;QString&quot; name=&quot;width_map_unit_scale&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; type=&quot;QString&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; type=&quot;QString&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" type="QString" name="lineSymbol"/>
          <Option value="0" type="double" name="minLength"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="minLengthMapUnitScale"/>
          <Option value="MM" type="QString" name="minLengthUnit"/>
          <Option value="0" type="double" name="offsetFromAnchor"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromAnchorMapUnitScale"/>
          <Option value="MM" type="QString" name="offsetFromAnchorUnit"/>
          <Option value="0" type="double" name="offsetFromLabel"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromLabelMapUnitScale"/>
          <Option value="MM" type="QString" name="offsetFromLabelUnit"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option value="0" type="QString" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <LinearlyInterpolatedDiagramRenderer diagramType="Histogram" lowerValue="0" upperWidth="5" attributeLegend="1" lowerHeight="0" upperHeight="5" lowerWidth="0" upperValue="0" classificationAttributeExpression="">
    <DiagramCategory scaleBasedVisibility="0" backgroundAlpha="255" maxScaleDenominator="1e+08" lineSizeScale="3x:0,0,0,0,0,0" diagramOrientation="Up" penAlpha="255" direction="1" backgroundColor="#ffffff" sizeType="MM" sizeScale="3x:0,0,0,0,0,0" width="15" height="15" scaleDependency="Area" spacingUnit="MM" stackedDiagramSpacing="0" barWidth="5" labelPlacementMethod="XHeight" rotationOffset="270" stackedDiagramSpacingUnitScale="3x:0,0,0,0,0,0" spacing="0" stackedDiagramSpacingUnit="MM" enabled="0" opacity="1" penColor="#000000" lineSizeType="MM" minimumSize="0" showAxis="0" minScaleDenominator="0" penWidth="0" spacingUnitScale="3x:0,0,0,0,0,0" stackedDiagramMode="Horizontal">
      <fontProperties description="Noto Sans,9,-1,5,50,0,0,0,0,0" strikethrough="0" underline="0" italic="0" style="" bold="0"/>
      <attribute color="#000000" colorOpacity="1" field="" label=""/>
      <axisSymbol>
        <symbol type="line" name="" force_rhr="0" frame_rate="10" is_animated="0" clip_to_extent="1" alpha="1">
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
          <layer id="{0bf6ea60-c1cb-436c-ad87-b875521a1c97}" class="SimpleLine" enabled="1" pass="0" locked="0">
            <Option type="Map">
              <Option value="0" type="QString" name="align_dash_pattern"/>
              <Option value="square" type="QString" name="capstyle"/>
              <Option value="5;2" type="QString" name="customdash"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="customdash_map_unit_scale"/>
              <Option value="MM" type="QString" name="customdash_unit"/>
              <Option value="0" type="QString" name="dash_pattern_offset"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="dash_pattern_offset_map_unit_scale"/>
              <Option value="MM" type="QString" name="dash_pattern_offset_unit"/>
              <Option value="0" type="QString" name="draw_inside_polygon"/>
              <Option value="bevel" type="QString" name="joinstyle"/>
              <Option value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" type="QString" name="line_color"/>
              <Option value="solid" type="QString" name="line_style"/>
              <Option value="0.26" type="QString" name="line_width"/>
              <Option value="MM" type="QString" name="line_width_unit"/>
              <Option value="0" type="QString" name="offset"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
              <Option value="MM" type="QString" name="offset_unit"/>
              <Option value="0" type="QString" name="ring_filter"/>
              <Option value="0" type="QString" name="trim_distance_end"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_end_map_unit_scale"/>
              <Option value="MM" type="QString" name="trim_distance_end_unit"/>
              <Option value="0" type="QString" name="trim_distance_start"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_start_map_unit_scale"/>
              <Option value="MM" type="QString" name="trim_distance_start_unit"/>
              <Option value="0" type="QString" name="tweak_dash_pattern_on_corners"/>
              <Option value="0" type="QString" name="use_custom_dash"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="width_map_unit_scale"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </LinearlyInterpolatedDiagramRenderer>
  <DiagramLayerSettings priority="0" linePlacementFlags="2" showAll="1" dist="0" obstacle="0" zIndex="0" placement="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="hole_id" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="data_set" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="x" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="y" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="z" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="srid" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="project_srid" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="eoh" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dip" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="azimuth" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="planned_eoh" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="loaded_by" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="load_date" configurationFlags="NoFlag">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="geom_trace" configurationFlags="NoFlag">
      <editWidget type="Binary">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="planned_trace" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="effective_geom" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="planned_geom" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="planned_x" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="planned_y" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="planned_z" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="planned_loc" configurationFlags="NoFlag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" index="0" field="hole_id"/>
    <alias name="" index="1" field="data_set"/>
    <alias name="" index="2" field="x"/>
    <alias name="" index="3" field="y"/>
    <alias name="" index="4" field="z"/>
    <alias name="" index="5" field="srid"/>
    <alias name="" index="6" field="project_srid"/>
    <alias name="" index="7" field="eoh"/>
    <alias name="" index="8" field="dip"/>
    <alias name="" index="9" field="azimuth"/>
    <alias name="" index="10" field="planned_eoh"/>
    <alias name="" index="11" field="loaded_by"/>
    <alias name="" index="12" field="load_date"/>
    <alias name="" index="13" field="geom_trace"/>
    <alias name="" index="14" field="planned_trace"/>
    <alias name="" index="15" field="effective_geom"/>
    <alias name="" index="16" field="planned_geom"/>
    <alias name="" index="17" field="planned_x"/>
    <alias name="" index="18" field="planned_y"/>
    <alias name="" index="19" field="planned_z"/>
    <alias name="" index="20" field="planned_loc"/>
  </aliases>
  <splitPolicies>
    <policy policy="Duplicate" field="hole_id"/>
    <policy policy="Duplicate" field="data_set"/>
    <policy policy="Duplicate" field="x"/>
    <policy policy="Duplicate" field="y"/>
    <policy policy="Duplicate" field="z"/>
    <policy policy="Duplicate" field="srid"/>
    <policy policy="Duplicate" field="project_srid"/>
    <policy policy="Duplicate" field="eoh"/>
    <policy policy="Duplicate" field="dip"/>
    <policy policy="Duplicate" field="azimuth"/>
    <policy policy="Duplicate" field="planned_eoh"/>
    <policy policy="Duplicate" field="loaded_by"/>
    <policy policy="Duplicate" field="load_date"/>
    <policy policy="Duplicate" field="geom_trace"/>
    <policy policy="Duplicate" field="planned_trace"/>
    <policy policy="Duplicate" field="effective_geom"/>
    <policy policy="Duplicate" field="planned_geom"/>
    <policy policy="Duplicate" field="planned_x"/>
    <policy policy="Duplicate" field="planned_y"/>
    <policy policy="Duplicate" field="planned_z"/>
    <policy policy="Duplicate" field="planned_loc"/>
  </splitPolicies>
  <duplicatePolicies>
    <policy policy="Duplicate" field="hole_id"/>
    <policy policy="Duplicate" field="data_set"/>
    <policy policy="Duplicate" field="x"/>
    <policy policy="Duplicate" field="y"/>
    <policy policy="Duplicate" field="z"/>
    <policy policy="Duplicate" field="srid"/>
    <policy policy="Duplicate" field="project_srid"/>
    <policy policy="Duplicate" field="eoh"/>
    <policy policy="Duplicate" field="dip"/>
    <policy policy="Duplicate" field="azimuth"/>
    <policy policy="Duplicate" field="planned_eoh"/>
    <policy policy="Duplicate" field="loaded_by"/>
    <policy policy="Duplicate" field="load_date"/>
    <policy policy="Duplicate" field="geom_trace"/>
    <policy policy="Duplicate" field="planned_trace"/>
    <policy policy="Duplicate" field="effective_geom"/>
    <policy policy="Duplicate" field="planned_geom"/>
    <policy policy="Duplicate" field="planned_x"/>
    <policy policy="Duplicate" field="planned_y"/>
    <policy policy="Duplicate" field="planned_z"/>
    <policy policy="Duplicate" field="planned_loc"/>
  </duplicatePolicies>
  <defaults>
    <default applyOnUpdate="0" field="hole_id" expression=""/>
    <default applyOnUpdate="0" field="data_set" expression=""/>
    <default applyOnUpdate="0" field="x" expression=""/>
    <default applyOnUpdate="0" field="y" expression=""/>
    <default applyOnUpdate="0" field="z" expression=""/>
    <default applyOnUpdate="0" field="srid" expression=""/>
    <default applyOnUpdate="0" field="project_srid" expression=""/>
    <default applyOnUpdate="0" field="eoh" expression=""/>
    <default applyOnUpdate="0" field="dip" expression=""/>
    <default applyOnUpdate="0" field="azimuth" expression=""/>
    <default applyOnUpdate="0" field="planned_eoh" expression=""/>
    <default applyOnUpdate="0" field="loaded_by" expression=""/>
    <default applyOnUpdate="0" field="load_date" expression=""/>
    <default applyOnUpdate="0" field="geom_trace" expression=""/>
    <default applyOnUpdate="0" field="planned_trace" expression=""/>
    <default applyOnUpdate="0" field="effective_geom" expression=""/>
    <default applyOnUpdate="0" field="planned_geom" expression=""/>
    <default applyOnUpdate="0" field="planned_x" expression=""/>
    <default applyOnUpdate="0" field="planned_y" expression=""/>
    <default applyOnUpdate="0" field="planned_z" expression=""/>
    <default applyOnUpdate="0" field="planned_loc" expression=""/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="hole_id" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="data_set" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="x" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="y" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="z" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="srid" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="project_srid" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="eoh" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="dip" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="azimuth" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="planned_eoh" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="loaded_by" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="load_date" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="geom_trace" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="planned_trace" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="effective_geom" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="planned_geom" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="planned_x" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="planned_y" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="planned_z" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" constraints="0" field="planned_loc" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="hole_id" desc=""/>
    <constraint exp="" field="data_set" desc=""/>
    <constraint exp="" field="x" desc=""/>
    <constraint exp="" field="y" desc=""/>
    <constraint exp="" field="z" desc=""/>
    <constraint exp="" field="srid" desc=""/>
    <constraint exp="" field="project_srid" desc=""/>
    <constraint exp="" field="eoh" desc=""/>
    <constraint exp="" field="dip" desc=""/>
    <constraint exp="" field="azimuth" desc=""/>
    <constraint exp="" field="planned_eoh" desc=""/>
    <constraint exp="" field="loaded_by" desc=""/>
    <constraint exp="" field="load_date" desc=""/>
    <constraint exp="" field="geom_trace" desc=""/>
    <constraint exp="" field="planned_trace" desc=""/>
    <constraint exp="" field="effective_geom" desc=""/>
    <constraint exp="" field="planned_geom" desc=""/>
    <constraint exp="" field="planned_x" desc=""/>
    <constraint exp="" field="planned_y" desc=""/>
    <constraint exp="" field="planned_z" desc=""/>
    <constraint exp="" field="planned_loc" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column type="field" width="-1" name="hole_id" hidden="0"/>
      <column type="field" width="-1" name="data_set" hidden="0"/>
      <column type="field" width="-1" name="x" hidden="0"/>
      <column type="field" width="-1" name="y" hidden="0"/>
      <column type="field" width="-1" name="z" hidden="0"/>
      <column type="field" width="-1" name="srid" hidden="0"/>
      <column type="field" width="-1" name="eoh" hidden="0"/>
      <column type="field" width="-1" name="loaded_by" hidden="0"/>
      <column type="field" width="-1" name="load_date" hidden="0"/>
      <column type="field" width="-1" name="geom_trace" hidden="0"/>
      <column type="field" width="-1" name="project_srid" hidden="0"/>
      <column type="field" width="-1" name="dip" hidden="0"/>
      <column type="field" width="-1" name="azimuth" hidden="0"/>
      <column type="field" width="-1" name="planned_eoh" hidden="0"/>
      <column type="field" width="-1" name="planned_trace" hidden="0"/>
      <column type="field" width="-1" name="effective_geom" hidden="0"/>
      <column type="field" width="-1" name="planned_geom" hidden="0"/>
      <column type="field" width="-1" name="planned_x" hidden="0"/>
      <column type="field" width="-1" name="planned_y" hidden="0"/>
      <column type="field" width="-1" name="planned_z" hidden="0"/>
      <column type="field" width="-1" name="planned_loc" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="azimuth" editable="1"/>
    <field name="data_set" editable="1"/>
    <field name="dip" editable="1"/>
    <field name="effective_geom" editable="1"/>
    <field name="eoh" editable="1"/>
    <field name="geom_trace" editable="1"/>
    <field name="hole_id" editable="1"/>
    <field name="load_date" editable="1"/>
    <field name="loaded_by" editable="1"/>
    <field name="planned_eoh" editable="1"/>
    <field name="planned_geom" editable="1"/>
    <field name="planned_loc" editable="1"/>
    <field name="planned_trace" editable="1"/>
    <field name="planned_x" editable="1"/>
    <field name="planned_y" editable="1"/>
    <field name="planned_z" editable="1"/>
    <field name="project_srid" editable="1"/>
    <field name="srid" editable="1"/>
    <field name="x" editable="1"/>
    <field name="y" editable="1"/>
    <field name="z" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="azimuth" labelOnTop="0"/>
    <field name="data_set" labelOnTop="0"/>
    <field name="dip" labelOnTop="0"/>
    <field name="effective_geom" labelOnTop="0"/>
    <field name="eoh" labelOnTop="0"/>
    <field name="geom_trace" labelOnTop="0"/>
    <field name="hole_id" labelOnTop="0"/>
    <field name="load_date" labelOnTop="0"/>
    <field name="loaded_by" labelOnTop="0"/>
    <field name="planned_eoh" labelOnTop="0"/>
    <field name="planned_geom" labelOnTop="0"/>
    <field name="planned_loc" labelOnTop="0"/>
    <field name="planned_trace" labelOnTop="0"/>
    <field name="planned_x" labelOnTop="0"/>
    <field name="planned_y" labelOnTop="0"/>
    <field name="planned_z" labelOnTop="0"/>
    <field name="project_srid" labelOnTop="0"/>
    <field name="srid" labelOnTop="0"/>
    <field name="x" labelOnTop="0"/>
    <field name="y" labelOnTop="0"/>
    <field name="z" labelOnTop="0"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="azimuth" reuseLastValue="0"/>
    <field name="data_set" reuseLastValue="0"/>
    <field name="dip" reuseLastValue="0"/>
    <field name="effective_geom" reuseLastValue="0"/>
    <field name="eoh" reuseLastValue="0"/>
    <field name="geom_trace" reuseLastValue="0"/>
    <field name="hole_id" reuseLastValue="0"/>
    <field name="load_date" reuseLastValue="0"/>
    <field name="loaded_by" reuseLastValue="0"/>
    <field name="planned_eoh" reuseLastValue="0"/>
    <field name="planned_geom" reuseLastValue="0"/>
    <field name="planned_loc" reuseLastValue="0"/>
    <field name="planned_trace" reuseLastValue="0"/>
    <field name="planned_x" reuseLastValue="0"/>
    <field name="planned_y" reuseLastValue="0"/>
    <field name="planned_z" reuseLastValue="0"/>
    <field name="project_srid" reuseLastValue="0"/>
    <field name="srid" reuseLastValue="0"/>
    <field name="x" reuseLastValue="0"/>
    <field name="y" reuseLastValue="0"/>
    <field name="z" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"hole_id"</previewExpression>
  <mapTip enabled="1">OGC_FID</mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
