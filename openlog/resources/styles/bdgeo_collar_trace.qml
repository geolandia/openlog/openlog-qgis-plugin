<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis autoRefreshMode="Disabled" labelsEnabled="1" autoRefreshTime="0" maxScale="0" simplifyLocal="1" simplifyAlgorithm="0" simplifyDrawingHints="1" minScale="100000000" readOnly="1" hasScaleBasedVisibilityFlag="0" simplifyDrawingTol="1" simplifyMaxScale="1" symbologyReferenceScale="-1" version="3.40.0-Bratislava" styleCategories="AllStyleCategories">
  <renderer-3d type="vector" layer="Borehole_trace_050f48e2_ad19_4444_8cca_e864f2e21b4e">
    <vector-layer-3d-tiling show-bounding-boxes="0" zoom-levels-count="3"/>
    <symbol type="line" material_type="simpleline">
      <data alt-binding="centroid" extrusion-height="0" width="8" offset="0" simple-lines="1" alt-clamping="absolute"/>
      <material ambient="26,26,26,255,rgb:0.10196078431372549,0.10196078431372549,0.10196078431372549,1">
        <data-defined-properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data-defined-properties>
      </material>
    </symbol>
  </renderer-3d>
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal endExpression="" endField="" durationField="id" mode="0" limitMode="0" accumulate="0" startExpression="" durationUnit="min" startField="drilling_date" enabled="0" fixedDuration="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation zoffset="0" binding="Centroid" type="IndividualFeatures" clamping="Absolute" showMarkerSymbolInSurfacePlots="0" extrusionEnabled="0" zscale="1" respectLayerSymbol="1" extrusion="0" symbology="Line">
    <data-defined-properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol clip_to_extent="1" is_animated="0" type="line" alpha="1" force_rhr="0" name="" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" class="SimpleLine" locked="0" enabled="1" id="{9d2ab464-0d03-419c-9270-24d9498937bd}">
          <Option type="Map">
            <Option type="QString" value="0" name="align_dash_pattern"/>
            <Option type="QString" value="square" name="capstyle"/>
            <Option type="QString" value="5;2" name="customdash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
            <Option type="QString" value="MM" name="customdash_unit"/>
            <Option type="QString" value="0" name="dash_pattern_offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
            <Option type="QString" value="0" name="draw_inside_polygon"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="164,113,88,255,rgb:0.64313725490196083,0.44313725490196076,0.34509803921568627,1" name="line_color"/>
            <Option type="QString" value="solid" name="line_style"/>
            <Option type="QString" value="0.6" name="line_width"/>
            <Option type="QString" value="MM" name="line_width_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="0" name="trim_distance_end"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_end_unit"/>
            <Option type="QString" value="0" name="trim_distance_start"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_start_unit"/>
            <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
            <Option type="QString" value="0" name="use_custom_dash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol clip_to_extent="1" is_animated="0" type="fill" alpha="1" force_rhr="0" name="" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" class="SimpleFill" locked="0" enabled="1" id="{28790fbd-1a14-4685-9eea-909b64b685bb}">
          <Option type="Map">
            <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
            <Option type="QString" value="164,113,88,255,rgb:0.64313725490196083,0.44313725490196076,0.34509803921568627,1" name="color"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="117,81,63,255,rgb:0.45882352941176469,0.31764705882352939,0.24705882352941178,1" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="solid" name="style"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol clip_to_extent="1" is_animated="0" type="marker" alpha="1" force_rhr="0" name="" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" class="SimpleMarker" locked="0" enabled="1" id="{936c3b4c-26c1-42ed-a885-9d8f45af24e1}">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="square" name="cap_style"/>
            <Option type="QString" value="164,113,88,255,rgb:0.64313725490196083,0.44313725490196076,0.34509803921568627,1" name="color"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="diamond" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="117,81,63,255,rgb:0.45882352941176469,0.31764705882352939,0.24705882352941178,1" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="3" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 referencescale="-1" type="singleSymbol" forceraster="0" symbollevels="0" enableorderby="0">
    <symbols>
      <symbol clip_to_extent="1" is_animated="0" type="line" alpha="1" force_rhr="0" name="0" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" class="SimpleLine" locked="0" enabled="1" id="{d1f2aefe-d3eb-4c81-a75c-16527bcf6ff8}">
          <Option type="Map">
            <Option type="QString" value="0" name="align_dash_pattern"/>
            <Option type="QString" value="square" name="capstyle"/>
            <Option type="QString" value="5;2" name="customdash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
            <Option type="QString" value="MM" name="customdash_unit"/>
            <Option type="QString" value="0" name="dash_pattern_offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
            <Option type="QString" value="0" name="draw_inside_polygon"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="255,127,0,255,rgb:1,0.49803921568627452,0,1" name="line_color"/>
            <Option type="QString" value="solid" name="line_style"/>
            <Option type="QString" value="0.36" name="line_width"/>
            <Option type="QString" value="MM" name="line_width_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="0" name="trim_distance_end"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_end_unit"/>
            <Option type="QString" value="0" name="trim_distance_start"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_start_unit"/>
            <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
            <Option type="QString" value="0" name="use_custom_dash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
    <data-defined-properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </data-defined-properties>
  </renderer-v2>
  <selection mode="Default">
    <selectionColor invalid="1"/>
    <selectionSymbol>
      <symbol clip_to_extent="1" is_animated="0" type="line" alpha="1" force_rhr="0" name="" frame_rate="10">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" class="SimpleLine" locked="0" enabled="1" id="{a0633b02-001e-4765-820a-b19be48ceb3e}">
          <Option type="Map">
            <Option type="QString" value="0" name="align_dash_pattern"/>
            <Option type="QString" value="square" name="capstyle"/>
            <Option type="QString" value="5;2" name="customdash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
            <Option type="QString" value="MM" name="customdash_unit"/>
            <Option type="QString" value="0" name="dash_pattern_offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
            <Option type="QString" value="0" name="draw_inside_polygon"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="line_color"/>
            <Option type="QString" value="solid" name="line_style"/>
            <Option type="QString" value="0.26" name="line_width"/>
            <Option type="QString" value="MM" name="line_width_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="0" name="trim_distance_end"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_end_unit"/>
            <Option type="QString" value="0" name="trim_distance_start"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_start_unit"/>
            <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
            <Option type="QString" value="0" name="use_custom_dash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </selectionSymbol>
  </selection>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style fontSizeMapUnitScale="3x:0,0,0,0,0,0" textOrientation="horizontal" forcedItalic="0" fontSize="10" isExpression="1" fontStrikeout="0" forcedBold="0" previewBkgrdColor="255,255,255,255,rgb:1,1,1,1" fontSizeUnit="Point" textColor="50,50,50,255,rgb:0.19607843137254902,0.19607843137254902,0.19607843137254902,1" fontLetterSpacing="0" tabStopDistanceUnit="Point" tabStopDistanceMapUnitScale="3x:0,0,0,0,0,0" fontWordSpacing="0" tabStopDistance="80" capitalization="0" fontItalic="0" fontKerning="1" multilineHeightUnit="Percentage" fontUnderline="0" fieldName="hole_id" legendString="Aa" namedStyle="Regular" useSubstitutions="0" textOpacity="1" multilineHeight="1" fontWeight="50" fontFamily="Liberation Sans" blendMode="0" allowHtml="0">
        <families/>
        <text-buffer bufferBlendMode="0" bufferOpacity="1" bufferColor="250,250,250,255,rgb:0.98039215686274506,0.98039215686274506,0.98039215686274506,1" bufferSize="1" bufferDraw="0" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferSizeUnits="MM" bufferNoFill="1" bufferJoinStyle="128"/>
        <text-mask maskType="0" maskEnabled="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskJoinStyle="128" maskedSymbolLayers="" maskSizeUnits="MM" maskOpacity="1" maskSize="1.5" maskSize2="1.5"/>
        <background shapeOpacity="1" shapeSizeX="0" shapeSizeType="0" shapeBorderWidth="0" shapeBorderColor="128,128,128,255,rgb:0.50196078431372548,0.50196078431372548,0.50196078431372548,1" shapeRadiiX="0" shapeType="0" shapeBlendMode="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeFillColor="255,255,255,255,rgb:1,1,1,1" shapeSizeY="0" shapeSizeUnit="Point" shapeRadiiY="0" shapeRadiiUnit="Point" shapeOffsetX="0" shapeRotation="0" shapeDraw="0" shapeOffsetY="0" shapeBorderWidthUnit="Point" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeRotationType="0" shapeJoinStyle="64" shapeSVGFile="" shapeOffsetUnit="Point" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0">
          <symbol clip_to_extent="1" is_animated="0" type="marker" alpha="1" force_rhr="0" name="markerSymbol" frame_rate="10">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer pass="0" class="SimpleMarker" locked="0" enabled="1" id="">
              <Option type="Map">
                <Option type="QString" value="0" name="angle"/>
                <Option type="QString" value="square" name="cap_style"/>
                <Option type="QString" value="152,125,183,255,rgb:0.59607843137254901,0.49019607843137253,0.71764705882352942,1" name="color"/>
                <Option type="QString" value="1" name="horizontal_anchor_point"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="circle" name="name"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="diameter" name="scale_method"/>
                <Option type="QString" value="2" name="size"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                <Option type="QString" value="MM" name="size_unit"/>
                <Option type="QString" value="1" name="vertical_anchor_point"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
          <symbol clip_to_extent="1" is_animated="0" type="fill" alpha="1" force_rhr="0" name="fillSymbol" frame_rate="10">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer pass="0" class="SimpleFill" locked="0" enabled="1" id="">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="255,255,255,255,rgb:1,1,1,1" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="128,128,128,255,rgb:0.50196078431372548,0.50196078431372548,0.50196078431372548,1" name="outline_color"/>
                <Option type="QString" value="no" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="Point" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowOffsetDist="1" shadowDraw="0" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadius="1.5" shadowScale="100" shadowUnder="0" shadowOffsetGlobal="1" shadowOffsetUnit="MM" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowColor="0,0,0,255,rgb:0,0,0,1" shadowRadiusUnit="MM" shadowOffsetAngle="135" shadowRadiusAlphaOnly="0" shadowOpacity="0.69999999999999996" shadowBlendMode="6"/>
        <dd_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format formatNumbers="0" rightDirectionSymbol=">" useMaxLineLengthForAutoWrap="1" plussign="0" autoWrapLength="0" addDirectionSymbol="0" placeDirectionSymbol="0" reverseDirectionSymbol="0" wrapChar="" multilineAlign="0" leftDirectionSymbol="&lt;" decimals="3"/>
      <placement overlapHandling="PreventOverlap" repeatDistance="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" placementFlags="10" overrunDistanceUnit="MM" geometryGeneratorEnabled="0" dist="0" placement="2" maximumDistanceMapUnitScale="3x:0,0,0,0,0,0" maximumDistanceUnit="MM" quadOffset="4" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" lineAnchorTextPoint="CenterOfText" repeatDistanceUnits="MM" lineAnchorType="0" layerType="LineGeometry" centroidWhole="0" centroidInside="0" yOffset="0" geometryGeneratorType="PointGeometry" xOffset="0" priority="5" offsetType="0" fitInPolygonOnly="0" lineAnchorPercent="0.5" rotationUnit="AngleDegrees" allowDegraded="0" maxCurvedCharAngleIn="25" overrunDistance="0" maximumDistance="0" distMapUnitScale="3x:0,0,0,0,0,0" offsetUnits="MM" maxCurvedCharAngleOut="-25" rotationAngle="0" lineAnchorClipping="0" polygonPlacementFlags="2" distUnits="MM" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" preserveRotation="1" geometryGenerator="" prioritization="PreferCloser" labelOffsetMapUnitScale="3x:0,0,0,0,0,0"/>
      <rendering fontMinPixelSize="3" drawLabels="1" obstacle="1" mergeLines="0" maxNumLabels="2000" scaleVisibility="0" obstacleType="1" fontLimitPixelSize="0" fontMaxPixelSize="10000" minFeatureSize="0" obstacleFactor="1" upsidedownLabels="0" scaleMax="0" scaleMin="0" limitNumLabels="0" unplacedVisibility="0" zIndex="0" labelPerPart="0"/>
      <dd_properties>
        <Option type="Map">
          <Option type="QString" value="" name="name"/>
          <Option name="properties"/>
          <Option type="QString" value="collection" name="type"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option type="QString" value="pole_of_inaccessibility" name="anchorPoint"/>
          <Option type="int" value="0" name="blendMode"/>
          <Option type="Map" name="ddProperties">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
          <Option type="bool" value="false" name="drawToAllParts"/>
          <Option type="QString" value="0" name="enabled"/>
          <Option type="QString" value="point_on_exterior" name="labelAnchorPoint"/>
          <Option type="QString" value="&lt;symbol clip_to_extent=&quot;1&quot; is_animated=&quot;0&quot; type=&quot;line&quot; alpha=&quot;1&quot; force_rhr=&quot;0&quot; name=&quot;symbol&quot; frame_rate=&quot;10&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; class=&quot;SimpleLine&quot; locked=&quot;0&quot; enabled=&quot;1&quot; id=&quot;{8cdf9ca1-f166-4788-ae30-8efd7b38f42a}&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;align_dash_pattern&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;square&quot; name=&quot;capstyle&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;5;2&quot; name=&quot;customdash&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;customdash_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;customdash_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;dash_pattern_offset&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;dash_pattern_offset_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;dash_pattern_offset_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;draw_inside_polygon&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;bevel&quot; name=&quot;joinstyle&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;60,60,60,255,rgb:0.23529411764705882,0.23529411764705882,0.23529411764705882,1&quot; name=&quot;line_color&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;solid&quot; name=&quot;line_style&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.3&quot; name=&quot;line_width&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;line_width_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;offset&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;offset_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;ring_filter&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;trim_distance_end&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;trim_distance_end_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;trim_distance_end_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;trim_distance_start&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;trim_distance_start_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;trim_distance_start_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;tweak_dash_pattern_on_corners&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;use_custom_dash&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;width_map_unit_scale&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" name="lineSymbol"/>
          <Option type="double" value="0" name="minLength"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="minLengthMapUnitScale"/>
          <Option type="QString" value="MM" name="minLengthUnit"/>
          <Option type="double" value="0" name="offsetFromAnchor"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale"/>
          <Option type="QString" value="MM" name="offsetFromAnchorUnit"/>
          <Option type="double" value="0" name="offsetFromLabel"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromLabelMapUnitScale"/>
          <Option type="QString" value="MM" name="offsetFromLabelUnit"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option type="int" value="0" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <LinearlyInterpolatedDiagramRenderer upperHeight="5" lowerHeight="0" attributeLegend="1" lowerValue="0" upperValue="0" lowerWidth="0" classificationAttributeExpression="" diagramType="Histogram" upperWidth="5">
    <DiagramCategory stackedDiagramMode="Horizontal" opacity="1" barWidth="5" scaleBasedVisibility="0" penColor="#000000" minScaleDenominator="0" spacingUnit="MM" minimumSize="0" maxScaleDenominator="1e+08" width="15" enabled="0" stackedDiagramSpacingUnit="MM" direction="0" backgroundColor="#ffffff" lineSizeType="MM" penAlpha="255" diagramOrientation="Up" rotationOffset="270" backgroundAlpha="255" sizeScale="3x:0,0,0,0,0,0" showAxis="1" spacingUnitScale="3x:0,0,0,0,0,0" sizeType="MM" scaleDependency="Area" stackedDiagramSpacingUnitScale="3x:0,0,0,0,0,0" lineSizeScale="3x:0,0,0,0,0,0" height="15" spacing="5" stackedDiagramSpacing="0" penWidth="0" labelPlacementMethod="XHeight">
      <fontProperties underline="0" bold="0" description="Ubuntu,11,-1,5,50,0,0,0,0,0" style="" italic="0" strikethrough="0"/>
      <attribute field="" color="#000000" colorOpacity="1" label=""/>
      <axisSymbol>
        <symbol clip_to_extent="1" is_animated="0" type="line" alpha="1" force_rhr="0" name="" frame_rate="10">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <layer pass="0" class="SimpleLine" locked="0" enabled="1" id="{383cae30-d812-469f-857d-3442a6c147dd}">
            <Option type="Map">
              <Option type="QString" value="0" name="align_dash_pattern"/>
              <Option type="QString" value="square" name="capstyle"/>
              <Option type="QString" value="5;2" name="customdash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
              <Option type="QString" value="MM" name="customdash_unit"/>
              <Option type="QString" value="0" name="dash_pattern_offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
              <Option type="QString" value="0" name="draw_inside_polygon"/>
              <Option type="QString" value="bevel" name="joinstyle"/>
              <Option type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1" name="line_color"/>
              <Option type="QString" value="solid" name="line_style"/>
              <Option type="QString" value="0.26" name="line_width"/>
              <Option type="QString" value="MM" name="line_width_unit"/>
              <Option type="QString" value="0" name="offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="offset_unit"/>
              <Option type="QString" value="0" name="ring_filter"/>
              <Option type="QString" value="0" name="trim_distance_end"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_end_unit"/>
              <Option type="QString" value="0" name="trim_distance_start"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_start_unit"/>
              <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
              <Option type="QString" value="0" name="use_custom_dash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </LinearlyInterpolatedDiagramRenderer>
  <DiagramLayerSettings placement="2" zIndex="0" priority="0" linePlacementFlags="18" showAll="1" obstacle="0" dist="0">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="NoFlag" name="id">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="site_id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="station_family">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="station_type">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="description">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="point">
      <editWidget type="Geometry">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="orig_srid">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="ground_altitude">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="dataset_id">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="total_depth">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="top_of_casing_altitude">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="casing_height">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="casing_internal_diameter">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="casing_external_diameter">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="driller">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="drilling_date">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="drilling_method">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="location">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="num_bss">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="usage">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="condition">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" index="0" name=""/>
    <alias field="site_id" index="1" name=""/>
    <alias field="station_family" index="2" name=""/>
    <alias field="station_type" index="3" name=""/>
    <alias field="description" index="4" name=""/>
    <alias field="name" index="5" name=""/>
    <alias field="point" index="6" name=""/>
    <alias field="orig_srid" index="7" name=""/>
    <alias field="ground_altitude" index="8" name=""/>
    <alias field="dataset_id" index="9" name=""/>
    <alias field="total_depth" index="10" name=""/>
    <alias field="top_of_casing_altitude" index="11" name=""/>
    <alias field="casing_height" index="12" name=""/>
    <alias field="casing_internal_diameter" index="13" name=""/>
    <alias field="casing_external_diameter" index="14" name=""/>
    <alias field="driller" index="15" name=""/>
    <alias field="drilling_date" index="16" name=""/>
    <alias field="drilling_method" index="17" name=""/>
    <alias field="location" index="18" name=""/>
    <alias field="num_bss" index="19" name=""/>
    <alias field="usage" index="20" name=""/>
    <alias field="condition" index="21" name=""/>
  </aliases>
  <splitPolicies>
    <policy field="id" policy="Duplicate"/>
    <policy field="site_id" policy="Duplicate"/>
    <policy field="station_family" policy="Duplicate"/>
    <policy field="station_type" policy="Duplicate"/>
    <policy field="description" policy="Duplicate"/>
    <policy field="name" policy="Duplicate"/>
    <policy field="point" policy="Duplicate"/>
    <policy field="orig_srid" policy="Duplicate"/>
    <policy field="ground_altitude" policy="Duplicate"/>
    <policy field="dataset_id" policy="Duplicate"/>
    <policy field="total_depth" policy="Duplicate"/>
    <policy field="top_of_casing_altitude" policy="Duplicate"/>
    <policy field="casing_height" policy="Duplicate"/>
    <policy field="casing_internal_diameter" policy="Duplicate"/>
    <policy field="casing_external_diameter" policy="Duplicate"/>
    <policy field="driller" policy="Duplicate"/>
    <policy field="drilling_date" policy="Duplicate"/>
    <policy field="drilling_method" policy="Duplicate"/>
    <policy field="location" policy="Duplicate"/>
    <policy field="num_bss" policy="Duplicate"/>
    <policy field="usage" policy="Duplicate"/>
    <policy field="condition" policy="Duplicate"/>
  </splitPolicies>
  <duplicatePolicies>
    <policy field="id" policy="Duplicate"/>
    <policy field="site_id" policy="Duplicate"/>
    <policy field="station_family" policy="Duplicate"/>
    <policy field="station_type" policy="Duplicate"/>
    <policy field="description" policy="Duplicate"/>
    <policy field="name" policy="Duplicate"/>
    <policy field="point" policy="Duplicate"/>
    <policy field="orig_srid" policy="Duplicate"/>
    <policy field="ground_altitude" policy="Duplicate"/>
    <policy field="dataset_id" policy="Duplicate"/>
    <policy field="total_depth" policy="Duplicate"/>
    <policy field="top_of_casing_altitude" policy="Duplicate"/>
    <policy field="casing_height" policy="Duplicate"/>
    <policy field="casing_internal_diameter" policy="Duplicate"/>
    <policy field="casing_external_diameter" policy="Duplicate"/>
    <policy field="driller" policy="Duplicate"/>
    <policy field="drilling_date" policy="Duplicate"/>
    <policy field="drilling_method" policy="Duplicate"/>
    <policy field="location" policy="Duplicate"/>
    <policy field="num_bss" policy="Duplicate"/>
    <policy field="usage" policy="Duplicate"/>
    <policy field="condition" policy="Duplicate"/>
  </duplicatePolicies>
  <defaults>
    <default applyOnUpdate="0" field="id" expression=""/>
    <default applyOnUpdate="0" field="site_id" expression=""/>
    <default applyOnUpdate="0" field="station_family" expression=""/>
    <default applyOnUpdate="0" field="station_type" expression=""/>
    <default applyOnUpdate="0" field="description" expression=""/>
    <default applyOnUpdate="0" field="name" expression=""/>
    <default applyOnUpdate="0" field="point" expression=""/>
    <default applyOnUpdate="0" field="orig_srid" expression=""/>
    <default applyOnUpdate="0" field="ground_altitude" expression=""/>
    <default applyOnUpdate="0" field="dataset_id" expression=""/>
    <default applyOnUpdate="0" field="total_depth" expression=""/>
    <default applyOnUpdate="0" field="top_of_casing_altitude" expression=""/>
    <default applyOnUpdate="0" field="casing_height" expression=""/>
    <default applyOnUpdate="0" field="casing_internal_diameter" expression=""/>
    <default applyOnUpdate="0" field="casing_external_diameter" expression=""/>
    <default applyOnUpdate="0" field="driller" expression=""/>
    <default applyOnUpdate="0" field="drilling_date" expression=""/>
    <default applyOnUpdate="0" field="drilling_method" expression=""/>
    <default applyOnUpdate="0" field="location" expression=""/>
    <default applyOnUpdate="0" field="num_bss" expression=""/>
    <default applyOnUpdate="0" field="usage" expression=""/>
    <default applyOnUpdate="0" field="condition" expression=""/>
  </defaults>
  <constraints>
    <constraint constraints="3" field="id" exp_strength="0" notnull_strength="1" unique_strength="1"/>
    <constraint constraints="0" field="site_id" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="station_family" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="station_type" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="description" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="name" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="point" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="orig_srid" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="ground_altitude" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="dataset_id" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="total_depth" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="top_of_casing_altitude" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="casing_height" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="casing_internal_diameter" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="casing_external_diameter" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="driller" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="drilling_date" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="drilling_method" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="location" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="num_bss" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="usage" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="condition" exp_strength="0" notnull_strength="0" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" exp="" desc=""/>
    <constraint field="site_id" exp="" desc=""/>
    <constraint field="station_family" exp="" desc=""/>
    <constraint field="station_type" exp="" desc=""/>
    <constraint field="description" exp="" desc=""/>
    <constraint field="name" exp="" desc=""/>
    <constraint field="point" exp="" desc=""/>
    <constraint field="orig_srid" exp="" desc=""/>
    <constraint field="ground_altitude" exp="" desc=""/>
    <constraint field="dataset_id" exp="" desc=""/>
    <constraint field="total_depth" exp="" desc=""/>
    <constraint field="top_of_casing_altitude" exp="" desc=""/>
    <constraint field="casing_height" exp="" desc=""/>
    <constraint field="casing_internal_diameter" exp="" desc=""/>
    <constraint field="casing_external_diameter" exp="" desc=""/>
    <constraint field="driller" exp="" desc=""/>
    <constraint field="drilling_date" exp="" desc=""/>
    <constraint field="drilling_method" exp="" desc=""/>
    <constraint field="location" exp="" desc=""/>
    <constraint field="num_bss" exp="" desc=""/>
    <constraint field="usage" exp="" desc=""/>
    <constraint field="condition" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column hidden="0" type="field" width="-1" name="id"/>
      <column hidden="0" type="field" width="-1" name="site_id"/>
      <column hidden="0" type="field" width="-1" name="station_family"/>
      <column hidden="0" type="field" width="-1" name="station_type"/>
      <column hidden="0" type="field" width="-1" name="description"/>
      <column hidden="0" type="field" width="-1" name="name"/>
      <column hidden="0" type="field" width="-1" name="point"/>
      <column hidden="0" type="field" width="-1" name="orig_srid"/>
      <column hidden="0" type="field" width="-1" name="ground_altitude"/>
      <column hidden="0" type="field" width="-1" name="dataset_id"/>
      <column hidden="0" type="field" width="-1" name="total_depth"/>
      <column hidden="0" type="field" width="-1" name="top_of_casing_altitude"/>
      <column hidden="0" type="field" width="-1" name="casing_height"/>
      <column hidden="0" type="field" width="-1" name="casing_internal_diameter"/>
      <column hidden="0" type="field" width="-1" name="casing_external_diameter"/>
      <column hidden="0" type="field" width="-1" name="driller"/>
      <column hidden="0" type="field" width="-1" name="drilling_date"/>
      <column hidden="0" type="field" width="-1" name="drilling_method"/>
      <column hidden="0" type="field" width="-1" name="location"/>
      <column hidden="0" type="field" width="-1" name="num_bss"/>
      <column hidden="0" type="field" width="-1" name="usage"/>
      <column hidden="0" type="field" width="-1" name="condition"/>
      <column hidden="1" type="actions" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="azimuth" editable="1"/>
    <field name="casing_external_diameter" editable="1"/>
    <field name="casing_height" editable="1"/>
    <field name="casing_internal_diameter" editable="1"/>
    <field name="condition" editable="1"/>
    <field name="data_set" editable="1"/>
    <field name="dataset_id" editable="1"/>
    <field name="description" editable="1"/>
    <field name="dip" editable="1"/>
    <field name="driller" editable="1"/>
    <field name="drilling_date" editable="1"/>
    <field name="drilling_method" editable="1"/>
    <field name="eoh" editable="1"/>
    <field name="geom" editable="1"/>
    <field name="ground_altitude" editable="1"/>
    <field name="hole_id" editable="1"/>
    <field name="id" editable="1"/>
    <field name="load_date" editable="1"/>
    <field name="loaded_by" editable="1"/>
    <field name="location" editable="1"/>
    <field name="name" editable="1"/>
    <field name="num_bss" editable="1"/>
    <field name="orig_srid" editable="1"/>
    <field name="planned_eoh" editable="1"/>
    <field name="planned_geom" editable="1"/>
    <field name="planned_trace" editable="1"/>
    <field name="point" editable="1"/>
    <field name="project_srid" editable="1"/>
    <field name="site_id" editable="1"/>
    <field name="srid" editable="1"/>
    <field name="station_family" editable="1"/>
    <field name="station_type" editable="1"/>
    <field name="top_of_casing_altitude" editable="1"/>
    <field name="total_depth" editable="1"/>
    <field name="usage" editable="1"/>
    <field name="x" editable="1"/>
    <field name="y" editable="1"/>
    <field name="z" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="azimuth"/>
    <field labelOnTop="0" name="casing_external_diameter"/>
    <field labelOnTop="0" name="casing_height"/>
    <field labelOnTop="0" name="casing_internal_diameter"/>
    <field labelOnTop="0" name="condition"/>
    <field labelOnTop="0" name="data_set"/>
    <field labelOnTop="0" name="dataset_id"/>
    <field labelOnTop="0" name="description"/>
    <field labelOnTop="0" name="dip"/>
    <field labelOnTop="0" name="driller"/>
    <field labelOnTop="0" name="drilling_date"/>
    <field labelOnTop="0" name="drilling_method"/>
    <field labelOnTop="0" name="eoh"/>
    <field labelOnTop="0" name="geom"/>
    <field labelOnTop="0" name="ground_altitude"/>
    <field labelOnTop="0" name="hole_id"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="load_date"/>
    <field labelOnTop="0" name="loaded_by"/>
    <field labelOnTop="0" name="location"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="num_bss"/>
    <field labelOnTop="0" name="orig_srid"/>
    <field labelOnTop="0" name="planned_eoh"/>
    <field labelOnTop="0" name="planned_geom"/>
    <field labelOnTop="0" name="planned_trace"/>
    <field labelOnTop="0" name="point"/>
    <field labelOnTop="0" name="project_srid"/>
    <field labelOnTop="0" name="site_id"/>
    <field labelOnTop="0" name="srid"/>
    <field labelOnTop="0" name="station_family"/>
    <field labelOnTop="0" name="station_type"/>
    <field labelOnTop="0" name="top_of_casing_altitude"/>
    <field labelOnTop="0" name="total_depth"/>
    <field labelOnTop="0" name="usage"/>
    <field labelOnTop="0" name="x"/>
    <field labelOnTop="0" name="y"/>
    <field labelOnTop="0" name="z"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="azimuth" reuseLastValue="0"/>
    <field name="casing_external_diameter" reuseLastValue="0"/>
    <field name="casing_height" reuseLastValue="0"/>
    <field name="casing_internal_diameter" reuseLastValue="0"/>
    <field name="condition" reuseLastValue="0"/>
    <field name="data_set" reuseLastValue="0"/>
    <field name="dataset_id" reuseLastValue="0"/>
    <field name="description" reuseLastValue="0"/>
    <field name="dip" reuseLastValue="0"/>
    <field name="driller" reuseLastValue="0"/>
    <field name="drilling_date" reuseLastValue="0"/>
    <field name="drilling_method" reuseLastValue="0"/>
    <field name="eoh" reuseLastValue="0"/>
    <field name="geom" reuseLastValue="0"/>
    <field name="ground_altitude" reuseLastValue="0"/>
    <field name="hole_id" reuseLastValue="0"/>
    <field name="id" reuseLastValue="0"/>
    <field name="load_date" reuseLastValue="0"/>
    <field name="loaded_by" reuseLastValue="0"/>
    <field name="location" reuseLastValue="0"/>
    <field name="name" reuseLastValue="0"/>
    <field name="num_bss" reuseLastValue="0"/>
    <field name="orig_srid" reuseLastValue="0"/>
    <field name="planned_eoh" reuseLastValue="0"/>
    <field name="planned_geom" reuseLastValue="0"/>
    <field name="planned_trace" reuseLastValue="0"/>
    <field name="point" reuseLastValue="0"/>
    <field name="project_srid" reuseLastValue="0"/>
    <field name="site_id" reuseLastValue="0"/>
    <field name="srid" reuseLastValue="0"/>
    <field name="station_family" reuseLastValue="0"/>
    <field name="station_type" reuseLastValue="0"/>
    <field name="top_of_casing_altitude" reuseLastValue="0"/>
    <field name="total_depth" reuseLastValue="0"/>
    <field name="usage" reuseLastValue="0"/>
    <field name="x" reuseLastValue="0"/>
    <field name="y" reuseLastValue="0"/>
    <field name="z" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"hole_id"</previewExpression>
  <mapTip enabled="1"></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
