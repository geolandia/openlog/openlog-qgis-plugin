from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QStyledItemDelegate, QLineEdit, QWidget


class PasswordItemDelegate(QStyledItemDelegate):
    def initStyleOption(self, option, index) -> None:
        """
        Define style option for password text display (with *****)

        Args:
            option: QStyleOptionViewItem
            index: QModelIndex
        """
        super().initStyleOption(option, index)
        style = option.widget.style() or QtWidgets.QApplication.style()
        hint = style.styleHint(QtWidgets.QStyle.SH_LineEdit_PasswordCharacter)
        option.text = chr(hint) * len(option.text)

    def createEditor(self, parent, option, index) -> QWidget:
        """
        Create an editor QLineEdit with password tesxt display (****)

        Args:
            parent: QWidget
            option: QStyleOptionViewItem
            index: QModelIndex

        Returns: QLineEdit with QLineEdit.Password echo mode

        """
        editor = QLineEdit(parent)
        editor.setEchoMode(QLineEdit.Password)
        return editor
