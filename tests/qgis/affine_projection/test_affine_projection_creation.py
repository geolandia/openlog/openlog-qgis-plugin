import numpy as np
from qgis.core import QgsCoordinateReferenceSystem

from openlog.core.affine_projection import (
    _create_affine_length_parameter,
    _create_affine_scale_parameter,
    _create_deriving_conversion,
    _create_wkt_base_and_cartesian,
    create_derived_projection_wkt,
)
from openlog.core.affine_transform import affine_transform


def test_affine_length_parameter_creation():
    expected_string = (
        'PARAMETER["A0",0.0000000000,LENGTHUNIT["metre",1],ID["EPSG",8623]]'
    )
    result = _create_affine_length_parameter(label="A0", value=0, epsg=8623)
    assert result == expected_string


def test_affine_scale_parameter_creation():
    expected_string = (
        'PARAMETER["A1",0.0000000000,SCALEUNIT["coefficient",1],ID["EPSG",8624]]'
    )
    result = _create_affine_scale_parameter(label="A1", value=0, epsg=8624)
    assert result == expected_string


def test_base_and_cartesion_creation_from_crs():
    crs = QgsCoordinateReferenceSystem("EPSG:7855")
    expected_base = (
        'PROJCRS["GDA2020 / MGA zone 55",'
        'BASEGEOGCRS["GDA2020",'
        'DATUM["Geocentric Datum of Australia 2020",'
        'ELLIPSOID["GRS 1980",6378137,298.257222101,'
        'LENGTHUNIT["metre",1]]],'
        'PRIMEM["Greenwich",0,ANGLEUNIT["degree",0.0174532925199433]],ID["EPSG",7844]],'
        'CONVERSION["Map Grid of Australia zone 55",METHOD["Transverse Mercator",ID["EPSG",9807]],'
        'PARAMETER["Latitude of natural origin",0,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8801]],'
        'PARAMETER["Longitude of natural origin",147,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8802]],'
        'PARAMETER["Scale factor at natural origin",0.9996,SCALEUNIT["unity",1],ID["EPSG",8805]],'
        'PARAMETER["False easting",500000,LENGTHUNIT["metre",1],ID["EPSG",8806]],'
        'PARAMETER["False northing",10000000,LENGTHUNIT["metre",1],ID["EPSG",8807]]'
        "]"
    )
    expected_cartesian = (
        "CS[Cartesian,2],"
        'AXIS["(E)",east,ORDER[1],LENGTHUNIT["metre",1]],'
        'AXIS["(N)",north,ORDER[2],LENGTHUNIT["metre",1]]'
    )
    base, cartesian = _create_wkt_base_and_cartesian(crs)
    assert base == expected_base
    assert cartesian == expected_cartesian


def get_expected_deriving_conversion(affine_param: np.array) -> str:
    expected_deviring_conversion = (
        'DERIVINGCONVERSION["Affine",'
        'METHOD["Affine parametric transformation",ID["EPSG",9624]'
        "]"
    )
    expected_deviring_conversion += (
        f',PARAMETER["A0",{affine_param[0]:.10f},LENGTHUNIT["metre",1],ID["EPSG",8623]]'
    )
    expected_deviring_conversion += f',PARAMETER["A1",{affine_param[1]:.10f},SCALEUNIT["coefficient",1],ID["EPSG",8624]]'
    expected_deviring_conversion += f',PARAMETER["A2",{affine_param[2]:.10f},SCALEUNIT["coefficient",1],ID["EPSG",8625]]'
    expected_deviring_conversion += (
        f',PARAMETER["B0",{affine_param[4]:.10f},LENGTHUNIT["metre",1],ID["EPSG",8639]]'
    )
    expected_deviring_conversion += f',PARAMETER["B1",{affine_param[5]:.10f},SCALEUNIT["coefficient",1],ID["EPSG",8640]]'
    expected_deviring_conversion += f',PARAMETER["B2",{affine_param[6]:.10f},SCALEUNIT["coefficient",1],ID["EPSG",8641]]'
    expected_deviring_conversion += "]"
    return expected_deviring_conversion


def test_derived_projection_creation():
    affine_param = np.random.rand(3 * 4)
    src = np.random.rand(10, 3)

    dst = affine_transform(src, affine_param)
    expected_deviring_conversion = get_expected_deriving_conversion(affine_param)

    deriving_conversion = _create_deriving_conversion(src, dst)
    assert deriving_conversion == expected_deviring_conversion


def test_affine_projection_creation():
    crs = QgsCoordinateReferenceSystem("EPSG:7855")
    bounds = crs.bounds()
    nb_values = 10
    x = np.random.uniform(bounds.xMinimum(), bounds.xMaximum(), nb_values)
    y = np.random.uniform(bounds.yMinimum(), bounds.yMaximum(), nb_values)
    z = np.random.uniform(0, 1000, nb_values)

    src = np.zeros((nb_values, 3))
    src[:, 0] = x
    src[:, 1] = y
    src[:, 2] = z
    affine_param = np.random.rand(3 * 4)
    dst = affine_transform(src, affine_param)

    expected_wkt = (
        'DERIVEDPROJCRS["test",BASEPROJCRS["GDA2020 / MGA zone 55",'
        'BASEGEOGCRS["GDA2020",'
        'DATUM["Geocentric Datum of Australia 2020",'
        'ELLIPSOID["GRS 1980",6378137,298.257222101,'
        'LENGTHUNIT["metre",1]]],'
        'PRIMEM["Greenwich",0,ANGLEUNIT["degree",0.0174532925199433]],ID["EPSG",7844]],'
        'CONVERSION["Map Grid of Australia zone 55",METHOD["Transverse Mercator",ID["EPSG",9807]],'
        'PARAMETER["Latitude of natural origin",0,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8801]],'
        'PARAMETER["Longitude of natural origin",147,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8802]],'
        'PARAMETER["Scale factor at natural origin",0.9996,SCALEUNIT["unity",1],ID["EPSG",8805]],'
        'PARAMETER["False easting",500000,LENGTHUNIT["metre",1],ID["EPSG",8806]],'
        'PARAMETER["False northing",10000000,LENGTHUNIT["metre",1],ID["EPSG",8807]]'
        "]]"
        f",{get_expected_deriving_conversion(affine_param)}"
        ",CS[Cartesian,2],"
        'AXIS["(E)",east,ORDER[1],LENGTHUNIT["metre",1]],'
        'AXIS["(N)",north,ORDER[2],LENGTHUNIT["metre",1]]'
        "]"
    )

    wkt = create_derived_projection_wkt("test", crs, src, dst)
    assert wkt == expected_wkt
