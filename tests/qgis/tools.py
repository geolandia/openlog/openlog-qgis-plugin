import json
from pathlib import Path
from string import ascii_lowercase

import numpy as np

from openlog.datamodel.assay.generic_assay import (
    AssayColumn,
    AssayDatabaseDefinition,
    AssayDataExtent,
    AssayDefinition,
    AssayDomainType,
    AssaySeriesType,
    GenericAssay,
)


def create_file(
    tmp_path: Path, filename: str, columns: [str], values: [[str]], delimiter: str
) -> Path:
    csv_file = tmp_path / filename
    with csv_file.open("a") as fp:
        fp.write(delimiter.join(columns) + "\n")
        for row in values:
            fp.write(delimiter.join(row) + "\n")
    return csv_file


def create_assay_discrete_definition_object(
    assay_name: str,
    schema: str,
    assay_domain: AssayDomainType,
    series_type: AssaySeriesType,
) -> (AssayDefinition, AssayDatabaseDefinition):
    """
    Create assay definition object AssayDefinition and AssayDatabaseDefinition for discrete data extent

    Args:
        assay_name: Assay name
        schema: schema name
        assay_domain: Assay domain
        series_type: Series type

    Returns: (AssayDefinition, AssayDatabaseDefinition)

    """
    definition = AssayDefinition(
        assay_name,
        assay_domain,
        AssayDataExtent.DISCRETE,
        {"y": AssayColumn("y", series_type)},
    )
    table = AssayDatabaseDefinition(
        table_name=assay_name,
        hole_id_col="hole",
        dataset_col="dataset",
        person_col="person",
        import_date_col="import_date",
        x_col="x",
        y_col={"y": "y"},
        schema=schema,
    )
    return definition, table


def create_assay_extended_definition_object(
    assay_name: str,
    schema: str,
    assay_domain: AssayDomainType,
    series_type: AssaySeriesType,
) -> (AssayDefinition, AssayDatabaseDefinition):
    """
    Create assay definition object AssayDefinition and AssayDatabaseDefinition for extended data extent

    Args:
        assay_name: Assay name
        schema: schema name
        assay_domain: Assay domain
        series_type: Series type

    Returns: (AssayDefinition, AssayDatabaseDefinition)

    """
    definition = AssayDefinition(
        assay_name,
        assay_domain,
        AssayDataExtent.EXTENDED,
        {"y": AssayColumn("y", series_type)},
    )

    table = AssayDatabaseDefinition(
        table_name=assay_name,
        hole_id_col="hole",
        dataset_col="dataset",
        person_col="person",
        import_date_col="import_date",
        x_col="x",
        x_end_col="x_end",
        y_col={"y": "y"},
        schema=schema,
    )
    return definition, table


data = {
    "from": [x for x in range(0, 150, 2)],
    "to": [x for x in range(2, 152, 2)],
    "num": np.random.RandomState(123).normal(loc=10, scale=5, size=75),
    "txt": np.random.RandomState(123).choice(
        a=[letter for letter in ascii_lowercase], size=75, replace=True
    ),
    "sph": [
        json.dumps(
            {
                "dip": int(dip),
                "azimuth": int(azimuth),
                "type": type_,
                "polarity": int(polarity),
            }
        )
        for dip, azimuth, type_, polarity in zip(
            np.random.RandomState(123).choice(a=range(-90, 90), size=75, replace=True),
            np.random.RandomState(123).choice(a=range(0, 360), size=75, replace=True),
            np.random.RandomState(123).choice(
                a=["LINE", "PLANE"], size=75, replace=True
            ),
            np.random.RandomState(123).choice(a=[0, 1], size=75, replace=True),
        )
    ],
}


def create_generic_assay(assay_def: AssayDefinition) -> GenericAssay:

    assay = GenericAssay(hole_id="MCDH001", assay_definition=assay_def)
    if assay_def.data_extent == AssayDataExtent.EXTENDED:
        assay.import_x_values = np.array([data.get("from"), data.get("to")]).T
    else:
        assay.import_x_values = np.array(data.get("from"))

    for column_name, assay_column in assay_def.columns.items():
        if assay_column.series_type == AssaySeriesType.NUMERICAL:
            assay.import_y_values[column_name] = data["num"]
        elif assay_column.series_type in (
            AssaySeriesType.CATEGORICAL,
            AssaySeriesType.NOMINAL,
        ):
            assay.import_y_values[column_name] = data["txt"]
        elif assay_column.series_type == AssaySeriesType.SPHERICAL:
            assay.import_y_values[column_name] = data["sph"]

    return assay
