from datetime import datetime, timedelta, timezone
from pathlib import Path

import numpy
import pandas
import pytest
from PyQt5.QtWidgets import QMessageBox

from openlog.datamodel.assay.generic_assay import AssaySeriesType
from openlog.gui.utils.column_definition import ColumnDefinition
from openlog.gui.utils.delimited_text_import_widget import DelimitedTextImportWidget


def check_dataframe_contents(
    df: pandas.DataFrame, expected_columns: [str], expected_value: [str]
):
    # Check column list is equal to expected columns
    assert df.columns.to_list() == expected_columns
    # Check nb row
    assert df.shape[0] == len(expected_value)
    # Check data row value
    for index, row in df.iterrows():
        numpy.testing.assert_equal(row.to_list(), expected_value[index])


def create_file(
    tmp_path: Path, columns: [str], values: [[str]], delimiter: str
) -> Path:
    csv_file = tmp_path / "temp.csv"
    with csv_file.open("a") as fp:
        fp.write(delimiter.join(columns) + "\n")
        for row in values:
            fp.write(delimiter.join(row) + "\n")
    return csv_file


import_params = (
    "expected_columns, column_mapping, delimiter, "
    "file_column, file_values, "
    "expected_value "
)

EXPECTED_COL = ["a", "b", "c"]
FILE_VALUES = [["a1", "b1", "c1"], ["a2", "b2", "c2"]]

import_value = [
    # CSV value without mapping
    (EXPECTED_COL, {}, ",", ["a", "b", "c"], FILE_VALUES, FILE_VALUES),
    # CSV value with mapping
    (
        EXPECTED_COL,
        {"a": "x", "b": "y", "c": "z"},
        ",",
        ["x", "y", "z"],
        FILE_VALUES,
        FILE_VALUES,
    ),
    # ; value with mapping
    (
        EXPECTED_COL,
        {"a": "x", "b": "y", "c": "z"},
        ";",
        ["x", "y", "z"],
        FILE_VALUES,
        FILE_VALUES,
    ),
    # | value with mapping
    (
        EXPECTED_COL,
        {"a": "x", "b": "y", "c": "z"},
        "|",
        ["x", "y", "z"],
        FILE_VALUES,
        FILE_VALUES,
    ),
    # \t value with mapping
    (
        EXPECTED_COL,
        {"a": "x", "b": "y", "c": "z"},
        "\t",
        ["x", "y", "z"],
        FILE_VALUES,
        FILE_VALUES,
    ),
    # space value with mapping
    (
        EXPECTED_COL,
        {"a": "x", "b": "y", "c": "z"},
        " ",
        ["x", "y", "z"],
        FILE_VALUES,
        FILE_VALUES,
    ),
    # . value with mapping
    (
        EXPECTED_COL,
        {"a": "x", "b": "y", "c": "z"},
        ".",
        ["x", "y", "z"],
        FILE_VALUES,
        FILE_VALUES,
    ),
    # . value with too much columns
    (
        EXPECTED_COL,
        {},
        ",",
        ["a", "b", "c", "d", "e"],
        [["a1", "b1", "c1", "d1", "e1"], ["a2", "b2", "c2", "d2", "e2"]],
        FILE_VALUES,
    ),
]


@pytest.mark.parametrize(
    import_params,
    import_value,
)
def test_read_file(
    qtbot,
    tmp_path: Path,
    expected_columns: [str],
    column_mapping: {str: str},
    file_column: [str],
    file_values: [[str]],
    delimiter: str,
    expected_value: [[str]],
):
    csv_file = create_file(tmp_path, file_column, file_values, delimiter)

    delimited_import_widget = DelimitedTextImportWidget()
    delimited_import_widget.set_delimiter(delimiter)
    columns = []
    for col in expected_columns:
        mapping = ColumnDefinition(
            column=col, mapping=col, series_type=AssaySeriesType.NOMINAL, optional=True
        )
        if col in column_mapping:
            mapping.mapping = column_mapping[col]
        columns.append(mapping)

    delimited_import_widget.set_column_definition(columns)
    delimited_import_widget.filename_edit.lineEdit().setText(str(csv_file))

    # Need to force update because signal for filename change is emitted only if widget lose focus
    delimited_import_widget._update_table_and_fields()

    df = delimited_import_widget.get_dataframe()

    check_dataframe_contents(df, expected_columns, expected_value)


tz = timezone(timedelta(hours=0))

date_import_params = "date_format, file_values, expected_value"

date_import_value = [
    ("%Y/%m/%d", [["2020/02/01"]], [[datetime(year=2020, month=2, day=1, tzinfo=tz)]]),
    ("%Y-%m-%d", [["2020-02-01"]], [[datetime(year=2020, month=2, day=1, tzinfo=tz)]]),
    (
        "auto",
        [["2020/02/01"]],
        [[datetime(year=2020, month=2, day=1, tzinfo=tz)]],
    ),  # Pandas can do automatic conversion
    ("%d/%m/%Y", [["01/02/2020"]], [[datetime(year=2020, month=2, day=1, tzinfo=tz)]]),
    ("%d-%m-%Y", [["01-02-2020"]], [[datetime(year=2020, month=2, day=1, tzinfo=tz)]]),
]


@pytest.mark.parametrize(
    date_import_params,
    date_import_value,
)
def test_valid_date_import(
    qtbot, tmp_path: Path, date_format: str, file_values: [[str]], expected_value: [[]]
):
    delimiter = ","
    file_column = ["date"]
    expected_columns = file_column
    csv_file = create_file(tmp_path, file_column, file_values, delimiter)

    delimited_import_widget = DelimitedTextImportWidget()
    delimited_import_widget.set_delimiter(delimiter)
    delimited_import_widget.set_column_definition(
        [
            ColumnDefinition(
                column=col,
                mapping=col,
                series_type=AssaySeriesType.DATETIME,
                optional=True,
            )
            for col in file_column
        ]
    )
    delimited_import_widget.set_date_format(date_format)
    delimited_import_widget.filename_edit.lineEdit().setText(str(csv_file))

    # Need to force update because signal for filename change is emitted only if widget lose focus
    delimited_import_widget._update_table_and_fields()

    df = delimited_import_widget.get_dataframe()

    check_dataframe_contents(df, expected_columns, expected_value)


invalid_date_import_value = [
    ("%d/%m/%Y", [["2020/02/01"]], [[numpy.nan]]),
    ("%d/%m/%Y", [["invalid str"]], [[numpy.nan]]),
    ("%Y/%m/%d", [["2020/31/01"]], [[numpy.nan]]),
    ("%Y/%m/%d", [["2020/02/31"]], [[numpy.nan]]),  # Invalid date, no 31 in february
]


@pytest.mark.parametrize(
    date_import_params,
    invalid_date_import_value,
)
def test_invalid_date_import(
    qtbot,
    tmp_path: Path,
    date_format: str,
    file_values: [[str]],
    expected_value: [[]],
    mocker,
):
    delimiter = ","
    file_column = ["date"]
    expected_columns = file_column
    csv_file = create_file(tmp_path, file_column, file_values, delimiter)

    delimited_import_widget = DelimitedTextImportWidget()
    delimited_import_widget.set_delimiter(delimiter)
    delimited_import_widget.set_column_definition(
        [
            ColumnDefinition(
                column=col,
                mapping=col,
                series_type=AssaySeriesType.DATETIME,
                optional=True,
            )
            for col in file_column
        ]
    )
    delimited_import_widget.set_date_format(date_format)
    delimited_import_widget.filename_edit.lineEdit().setText(str(csv_file))

    # Need to force update because signal for filename change is emitted only if widget lose focus
    delimited_import_widget._update_table_and_fields()

    df = delimited_import_widget.get_dataframe()

    check_dataframe_contents(df, expected_columns, expected_value)


def test_val_conversion(qtbot, tmp_path):
    def _plus_one(col: str, _df: pandas.DataFrame) -> pandas.Series:
        val = _df[col].astype(float)
        val = val + 1
        return val

    delimiter = ","
    file_column = ["val", "other_val"]
    expected_columns = file_column
    file_values = [["5", "7"], ["6", "8"]]
    csv_file = create_file(tmp_path, file_column, file_values, delimiter)

    delimited_import_widget = DelimitedTextImportWidget()
    delimited_import_widget.set_delimiter(delimiter)
    delimited_import_widget.set_column_definition(
        [
            ColumnDefinition(
                column=col,
                mapping=col,
                series_type=AssaySeriesType.NUMERICAL,
                optional=True,
            )
            for col in file_column
        ]
    )
    delimited_import_widget.set_column_conversion(
        {"val": _plus_one, "other_val": _plus_one}
    )
    delimited_import_widget.filename_edit.lineEdit().setText(str(csv_file))

    # Need to force update because signal for filename change is emitted only if widget lose focus
    delimited_import_widget._update_table_and_fields()

    df = delimited_import_widget.get_dataframe()

    expected_value = [[6, 8], [7, 9]]

    check_dataframe_contents(df, expected_columns, expected_value)


def test_multiple_col_conversion(qtbot, tmp_path):
    def _sum_col(col: str, _df: pandas.DataFrame) -> pandas.Series:
        val = _df["val"].astype(float) + _df["other_val"].astype(float)
        return val

    delimiter = ","
    file_column = ["val", "other_val", "sum"]
    expected_columns = file_column
    file_values = [["5", "7"], ["6", "8"]]
    csv_file = create_file(tmp_path, file_column, file_values, delimiter)

    delimited_import_widget = DelimitedTextImportWidget()
    delimited_import_widget.set_delimiter(delimiter)
    delimited_import_widget.set_column_definition(
        [
            ColumnDefinition(
                column=col,
                mapping=col,
                series_type=AssaySeriesType.NUMERICAL,
                optional=True,
            )
            for col in file_column
        ]
    )
    delimited_import_widget.set_column_conversion({"sum": _sum_col})
    delimited_import_widget.filename_edit.lineEdit().setText(str(csv_file))

    # Need to force update because signal for filename change is emitted only if widget lose focus
    delimited_import_widget._update_table_and_fields()

    df = delimited_import_widget.get_dataframe()

    expected_value = [[5, 7, 5 + 7], [6, 8, 6 + 8]]

    check_dataframe_contents(df, expected_columns, expected_value)


def test_invalid_col_conversion(qtbot, tmp_path, mocker):
    def _invalid_conv(col: str, _df: pandas.DataFrame) -> pandas.Series:
        raise Exception("Invalid conversion")

    delimiter = ","
    file_column = ["val", "other_val"]
    expected_columns = file_column
    file_values = [["5", "7"], ["6", "8"]]
    csv_file = create_file(tmp_path, file_column, file_values, delimiter)

    delimited_import_widget = DelimitedTextImportWidget()
    delimited_import_widget.set_delimiter(delimiter)
    delimited_import_widget.set_column_definition(
        [
            ColumnDefinition(
                column=col,
                mapping=col,
                series_type=AssaySeriesType.NUMERICAL,
                optional=True,
            )
            for col in file_column
        ]
    )
    delimited_import_widget.set_column_conversion({"other_val": _invalid_conv})
    delimited_import_widget.filename_edit.lineEdit().setText(str(csv_file))

    # Need to force update because signal for filename change is emitted only if widget lose focus
    msg_box_mock = mocker.patch.object(
        QMessageBox, "warning", return_value=QMessageBox.Ok
    )
    delimited_import_widget._update_table_and_fields()
    msg_box_mock.assert_called_once()

    df = delimited_import_widget.get_dataframe()

    expected_value = [[5, None], [6, None]]

    check_dataframe_contents(df, expected_columns, expected_value)


def test_read_quoted_file(qtbot, tmp_path: Path):
    expected_columns = ["a", "b", "c"]
    delimiter = ","
    file_column = expected_columns
    file_values = [["a", '"a,b,c"', "c"]]
    expected_value = [["a", "a,b,c", "c"]]
    csv_file = create_file(tmp_path, file_column, file_values, delimiter)

    delimited_import_widget = DelimitedTextImportWidget()
    delimited_import_widget.set_delimiter(delimiter)
    delimited_import_widget.set_column_definition(
        [
            ColumnDefinition(
                column=col,
                mapping=col,
                series_type=AssaySeriesType.NOMINAL,
                optional=True,
            )
            for col in file_column
        ]
    )
    delimited_import_widget.filename_edit.lineEdit().setText(str(csv_file))

    # Need to force update because signal for filename change is emitted only if widget lose focus
    delimited_import_widget._update_table_and_fields()

    df = delimited_import_widget.get_dataframe()

    check_dataframe_contents(df, expected_columns, expected_value)
