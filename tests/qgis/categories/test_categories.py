from openlog.datamodel.assay.categories import CategoriesTableDefinition
from openlog.datamodel.connection.openlog_connection import OpenLogConnection


def test_categories_table_creation(test_connection: OpenLogConnection):
    assert test_connection.get_categories_iface().can_import_categories()

    categories_table = CategoriesTableDefinition(name="test", table_name="test")

    current_categories_table = (
        test_connection.get_categories_iface().get_available_categories_table()
    )
    test_connection.get_categories_iface().import_categories_table([categories_table])

    available_table = (
        test_connection.get_categories_iface().get_available_categories_table()
    )
    assert available_table == current_categories_table + [categories_table]


"""
def test_categories_table_creation_rollback(test_connection: OpenLogConnection):
    assert test_connection.get_categories_iface().can_import_categories()

    categories_table = CategoriesTableDefinition(name="test", table_name="test")

    current_categories_table = (
        test_connection.get_categories_iface().get_available_categories_table()
    )
    test_connection.get_categories_iface().import_categories_table([categories_table])

    test_connection.rollback()

    available_table = (
        test_connection.get_categories_iface().get_available_categories_table()
    )
    assert available_table == current_categories_table
"""


def test_categories_data_import(test_connection: OpenLogConnection):
    categories_table = CategoriesTableDefinition(name="test", table_name="test")
    test_connection.get_categories_iface().import_categories_table([categories_table])

    data = ["1", "2", "3"]
    test_connection.get_categories_iface().import_categories_data(
        categories_table, data
    )

    assert (
        test_connection.get_categories_iface().get_available_categories(
            categories_table
        )
        == data
    )


def test_categories_data_duplicate_values_import(test_connection: OpenLogConnection):
    categories_table = CategoriesTableDefinition(name="test", table_name="test")
    test_connection.get_categories_iface().import_categories_table([categories_table])

    data = ["4", "2", "4", "1"]
    test_connection.get_categories_iface().import_categories_data(
        categories_table, data
    )

    assert test_connection.get_categories_iface().get_available_categories(
        categories_table
    ) == ["1", "2", "4"]


def test_categories_data_import_values_already_in_table(
    test_connection: OpenLogConnection,
):
    categories_table = CategoriesTableDefinition(name="test", table_name="test")
    test_connection.get_categories_iface().import_categories_table([categories_table])

    data = ["5", "6", "7", "8"]
    test_connection.get_categories_iface().import_categories_data(
        categories_table, data
    )

    data = ["5", "6", "7", "8"]
    test_connection.get_categories_iface().import_categories_data(
        categories_table, data
    )

    assert test_connection.get_categories_iface().get_available_categories(
        categories_table
    ) == [
        "5",
        "6",
        "7",
        "8",
    ]

    data = ["1", "2", "3", "4"]
    test_connection.get_categories_iface().import_categories_data(
        categories_table, data
    )

    assert test_connection.get_categories_iface().get_available_categories(
        categories_table
    ) == [
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
    ]
