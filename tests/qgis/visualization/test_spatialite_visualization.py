from string import ascii_lowercase

import pytest
from xplordb.datamodel.collar import Collar
from xplordb.datamodel.survey import Survey

from openlog.datamodel.assay.categories import CategoriesTableDefinition
from openlog.datamodel.assay.generic_assay import AssayDomainType, AssaySeriesType
from openlog.gui.pyqtgraph.CustomBarGraphItem import CustomBarGraphItem
from openlog.plugins.visualization.categorical.item import (
    CategoricalBarGraphItem,
    CategoricalScatterPlotItem,
)
from openlog.plugins.visualization.numerical.item import (
    AssayBarGraphItem,
    AssayPlotDataItem,
)
from openlog.plugins.visualization.structural.item import StructuralSymbolItem
from tests.qgis.tools import (
    create_assay_discrete_definition_object,
    create_assay_extended_definition_object,
    create_generic_assay,
)


# to avoid "unable to convert a QVariant back to a Python object" error
@pytest.fixture(scope="module", autouse=True)
def fix_pyqtgraph_pytest():
    from pyqtgraph.graphicsItems.GraphicsObject import GraphicsObject

    def newFunc(cls, change, value):
        return

    GraphicsObject.itemChange = newFunc


# import collar
@pytest.fixture()
def plugin_c(plugin_spatialite_create):
    collars = [
        Collar(
            hole_id="MCDH001",
            data_set="ds",
            loaded_by="test",
            x=200687.09,
            y=7609858.79,
            z=463.33,
            srid=28351,
            eoh=150.2,
            planned_eoh=150.2,
            planned_x=200687.09,
            planned_y=7609858.79,
            planned_z=463.33,
            dip=-60,
            azimuth=310,
            project_srid=3857,
        )
    ]
    plugin_spatialite_create.openlog_connection.get_write_iface().import_collar(collars)
    return plugin_spatialite_create


# import surveys
@pytest.fixture()
def plugin_cs(plugin_c):
    surveys = [
        Survey(
            hole_id="MCDH001",
            data_set="ds",
            loaded_by="test",
            depth=19,
            dip=-59.6,
            azimuth=308.8,
        ),
        Survey(
            hole_id="MCDH001",
            data_set="ds",
            loaded_by="test",
            depth=49,
            dip=-60.1,
            azimuth=308.3,
        ),
        Survey(
            hole_id="MCDH001",
            data_set="ds",
            loaded_by="test",
            depth=79,
            dip=-60.7,
            azimuth=309.4,
        ),
        Survey(
            hole_id="MCDH001",
            data_set="ds",
            loaded_by="test",
            depth=109,
            dip=-61.4,
            azimuth=307.4,
        ),
        Survey(
            hole_id="MCDH001",
            data_set="ds",
            loaded_by="test",
            depth=139,
            dip=-63,
            azimuth=306.8,
        ),
    ]
    plugin_c.openlog_connection.get_write_iface().import_surveys(surveys)
    return plugin_c


# desurvey
@pytest.fixture()
def plugin_cst(plugin_cs):
    plugin_cs._selected_collar_desurveying(hole_ids=["MCDH001"])
    trace_length, planned_length = plugin_cs.openlog_connection.session.execute(
        "select st_3dlength(st_transform(geom_trace, srid)), st_3dlength(st_transform(planned_trace, srid)) from collar"
    ).fetchone()
    # test trace length in original srid with 1cm tolerance
    assert 150.2 == pytest.approx(trace_length, abs=0.01)
    assert 150.2 == pytest.approx(planned_length, abs=0.01)
    return plugin_cs


# assay import
## numerical
@pytest.fixture()
def plugin_cst_num(plugin_cst):
    # discrete
    assay_def, assay_db_def = create_assay_discrete_definition_object(
        assay_name="discrete_num",
        schema="",
        assay_domain=AssayDomainType.DEPTH,
        series_type=AssaySeriesType.NUMERICAL,
    )
    plugin_cst.openlog_connection.get_assay_iface().add_assay_table(
        assay_def, assay_db_def
    )
    plugin_cst.openlog_connection.get_assay_iface().import_assay_data(
        [create_generic_assay(assay_def)]
    )
    plugin_cst.openlog_connection.commit()

    # extended
    assay_def, assay_db_def = create_assay_extended_definition_object(
        assay_name="extended_num",
        schema="",
        assay_domain=AssayDomainType.DEPTH,
        series_type=AssaySeriesType.NUMERICAL,
    )
    plugin_cst.openlog_connection.get_assay_iface().add_assay_table(
        assay_def, assay_db_def
    )
    plugin_cst.openlog_connection.get_assay_iface().import_assay_data(
        [create_generic_assay(assay_def)]
    )
    plugin_cst.openlog_connection.commit()

    return plugin_cst


## nominal
@pytest.fixture()
def plugin_cst_num_nom(plugin_cst_num):
    # discrete
    assay_def, assay_db_def = create_assay_discrete_definition_object(
        assay_name="discrete_nominal",
        schema="",
        assay_domain=AssayDomainType.DEPTH,
        series_type=AssaySeriesType.NOMINAL,
    )
    plugin_cst_num.openlog_connection.get_assay_iface().add_assay_table(
        assay_def, assay_db_def
    )
    plugin_cst_num.openlog_connection.get_assay_iface().import_assay_data(
        [create_generic_assay(assay_def)]
    )
    plugin_cst_num.openlog_connection.commit()

    # extended
    assay_def, assay_db_def = create_assay_extended_definition_object(
        assay_name="extended_nominal",
        schema="",
        assay_domain=AssayDomainType.DEPTH,
        series_type=AssaySeriesType.NOMINAL,
    )
    plugin_cst_num.openlog_connection.get_assay_iface().add_assay_table(
        assay_def, assay_db_def
    )
    plugin_cst_num.openlog_connection.get_assay_iface().import_assay_data(
        [create_generic_assay(assay_def)]
    )
    plugin_cst_num.openlog_connection.commit()

    return plugin_cst_num


## categorical
@pytest.fixture()
def plugin_cst_num_nom_cat(plugin_cst_num_nom):

    # discrete
    assay_def, assay_db_def = create_assay_discrete_definition_object(
        assay_name="discrete_cat",
        schema="",
        assay_domain=AssayDomainType.DEPTH,
        series_type=AssaySeriesType.CATEGORICAL,
    )
    # new category table
    plugin_cst_num_nom.openlog_connection.get_categories_iface().import_categories_table(
        [CategoriesTableDefinition(name="letter", table_name="letter")]
    )
    # import categories
    plugin_cst_num_nom.openlog_connection.get_categories_iface().import_categories_data(
        CategoriesTableDefinition(name="letter", table_name="letter"),
        [letter for letter in ascii_lowercase],
    )
    plugin_cst_num_nom.openlog_connection.commit()
    plugin_cst_num_nom.openlog_connection.get_assay_iface().add_assay_table(
        assay_def, assay_db_def
    )
    plugin_cst_num_nom.openlog_connection.get_assay_iface().import_assay_data(
        [create_generic_assay(assay_def)]
    )
    plugin_cst_num_nom.openlog_connection.commit()

    # extended
    assay_def, assay_db_def = create_assay_extended_definition_object(
        assay_name="extended_cat",
        schema="",
        assay_domain=AssayDomainType.DEPTH,
        series_type=AssaySeriesType.CATEGORICAL,
    )

    plugin_cst_num_nom.openlog_connection.get_assay_iface().add_assay_table(
        assay_def, assay_db_def
    )
    plugin_cst_num_nom.openlog_connection.get_assay_iface().import_assay_data(
        [create_generic_assay(assay_def)]
    )
    plugin_cst_num_nom.openlog_connection.commit()

    return plugin_cst_num_nom


## spherical
@pytest.fixture()
def plugin_cst_num_nom_cat_sph(plugin_cst_num_nom_cat):
    # discrete
    assay_def, assay_db_def = create_assay_discrete_definition_object(
        assay_name="discrete_spherical",
        schema="",
        assay_domain=AssayDomainType.DEPTH,
        series_type=AssaySeriesType.SPHERICAL,
    )
    plugin_cst_num_nom_cat.openlog_connection.get_assay_iface().add_assay_table(
        assay_def, assay_db_def
    )
    plugin_cst_num_nom_cat.openlog_connection.get_assay_iface().import_assay_data(
        [create_generic_assay(assay_def)]
    )
    plugin_cst_num_nom_cat.openlog_connection.commit()

    # extended
    assay_def, assay_db_def = create_assay_extended_definition_object(
        assay_name="extended_spherical",
        schema="",
        assay_domain=AssayDomainType.DEPTH,
        series_type=AssaySeriesType.SPHERICAL,
    )
    plugin_cst_num_nom_cat.openlog_connection.get_assay_iface().add_assay_table(
        assay_def, assay_db_def
    )
    plugin_cst_num_nom_cat.openlog_connection.get_assay_iface().import_assay_data(
        [create_generic_assay(assay_def)]
    )
    plugin_cst_num_nom_cat.openlog_connection.commit()

    return plugin_cst_num_nom_cat


def test_visualization_num(plugin_cst_num_nom_cat_sph):
    assay_definitions = (
        plugin_cst_num_nom_cat_sph.openlog_connection.get_assay_iface().get_all_available_assay_definitions()
    )
    plugin_cst_num_nom_cat_sph.depth_assay_visu_widget._add_selected_assay_and_collar(
        assay_list=["discrete_num", "extended_num"],
        selected_hole_id=["MCDH001"],
        assay_definitions=assay_definitions,
    )
    # check item exists
    assay_configs = (
        plugin_cst_num_nom_cat_sph.depth_assay_visu_widget._visualization_config.list
    )
    for assay_config in assay_configs:
        if assay_config.hole_id != "" and assay_config.assay_name == "discrete_num":
            items = assay_config.column_config["y"].plot_widget.plotItem.items
            item = items[0]
            assert isinstance(item, AssayPlotDataItem)

        if assay_config.hole_id != "" and assay_config.assay_name == "extended_num":
            items = assay_config.column_config["y"].plot_widget.plotItem.items
            item = items[0]
            assert isinstance(item, AssayBarGraphItem)

    # switch altitude
    plugin_cst_num_nom_cat_sph.depth_assay_visu_widget.switch_altitude_checkbox.setChecked(
        True
    )
    plugin_cst_num_nom_cat_sph.depth_assay_visu_widget._switch_altitude()


def test_visualization_cat(plugin_cst_num_nom_cat_sph):
    assay_definitions = (
        plugin_cst_num_nom_cat_sph.openlog_connection.get_assay_iface().get_all_available_assay_definitions()
    )
    plugin_cst_num_nom_cat_sph.depth_assay_visu_widget._add_selected_assay_and_collar(
        assay_list=["discrete_cat", "extended_cat"],
        selected_hole_id=["MCDH001"],
        assay_definitions=assay_definitions,
    )
    # check item exists
    assay_configs = (
        plugin_cst_num_nom_cat_sph.depth_assay_visu_widget._visualization_config.list
    )
    for assay_config in assay_configs:
        if assay_config.hole_id != "" and assay_config.assay_name == "discrete_cat":
            items = assay_config.column_config["y"].plot_widget.plotItem.items
            item = items[0]
            assert isinstance(item, CategoricalScatterPlotItem)

        if assay_config.hole_id != "" and assay_config.assay_name == "extended_cat":
            items = assay_config.column_config["y"].plot_widget.plotItem.items
            assert any([isinstance(item, CategoricalBarGraphItem) for item in items])

    # switch altitude
    plugin_cst_num_nom_cat_sph.depth_assay_visu_widget.switch_altitude_checkbox.setChecked(
        True
    )
    plugin_cst_num_nom_cat_sph.depth_assay_visu_widget._switch_altitude()


def test_visualization_nom(plugin_cst_num_nom_cat_sph):
    assay_definitions = (
        plugin_cst_num_nom_cat_sph.openlog_connection.get_assay_iface().get_all_available_assay_definitions()
    )
    plugin_cst_num_nom_cat_sph.depth_assay_visu_widget._add_selected_assay_and_collar(
        assay_list=["extended_nominal"],
        selected_hole_id=["MCDH001"],
        assay_definitions=assay_definitions,
    )
    # check item exists
    assay_configs = (
        plugin_cst_num_nom_cat_sph.depth_assay_visu_widget._visualization_config.list
    )
    for assay_config in assay_configs:

        if assay_config.hole_id != "" and assay_config.assay_name == "extended_nominal":
            items = assay_config.column_config["y"].plot_widget.plotItem.items
            assert any([isinstance(item, CustomBarGraphItem) for item in items])

    # switch altitude
    plugin_cst_num_nom_cat_sph.depth_assay_visu_widget.switch_altitude_checkbox.setChecked(
        True
    )
    plugin_cst_num_nom_cat_sph.depth_assay_visu_widget._switch_altitude()


def test_visualization_sph(plugin_cst_num_nom_cat_sph):
    assay_definitions = (
        plugin_cst_num_nom_cat_sph.openlog_connection.get_assay_iface().get_all_available_assay_definitions()
    )
    plugin_cst_num_nom_cat_sph.depth_assay_visu_widget._add_selected_assay_and_collar(
        assay_list=["discrete_spherical", "extended_spherical"],
        selected_hole_id=["MCDH001"],
        assay_definitions=assay_definitions,
    )
    # check item exists
    assay_configs = (
        plugin_cst_num_nom_cat_sph.depth_assay_visu_widget._visualization_config.list
    )
    for assay_config in assay_configs:
        if (
            assay_config.hole_id != ""
            and assay_config.assay_name == "discrete_spherical"
        ):
            items = assay_config.column_config["y"].plot_widget.plotItem.items
            item = items[0]
            assert isinstance(item, StructuralSymbolItem)

        if (
            assay_config.hole_id != ""
            and assay_config.assay_name == "extended_spherical"
        ):
            items = assay_config.column_config["y"].plot_widget.plotItem.items
            item = items[0]
            assert isinstance(item, StructuralSymbolItem)

    # switch altitude
    plugin_cst_num_nom_cat_sph.depth_assay_visu_widget.switch_altitude_checkbox.setChecked(
        True
    )
    plugin_cst_num_nom_cat_sph.depth_assay_visu_widget._switch_altitude()
