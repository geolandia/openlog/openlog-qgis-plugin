import pytest
from PyQt5.QtWidgets import QMessageBox

from openlog.datamodel.assay.generic_assay import (
    AssayDataExtent,
    AssayDomainType,
    AssaySeriesType,
)
from openlog.datamodel.connection.openlog_connection import OpenLogConnection
from openlog.gui.import_assay.import_assay import AssayImportWizard
from tests.qgis.tools import (
    create_assay_discrete_definition_object,
    create_assay_extended_definition_object,
    create_file,
)


@pytest.fixture()
def import_assay_wizard(qtbot, test_connection: OpenLogConnection) -> AssayImportWizard:
    """
    Fixture to create a AssayImportWizard

    Args:
        test_connection: OpenLogConnection
        qtbot: qtbot fixture
    """
    widget = AssayImportWizard(test_connection)
    qtbot.addWidget(widget)
    yield widget
    test_connection.rollback()


# ---------------- TESTS FOR SELECTION PAGE ----------------------------------
@pytest.fixture()
def assay_selection_page(import_assay_wizard):
    """
    Fixture to get a AssayImportWizard assay selection page

    Args:
        import_assay_wizard: AssayImportWizard
    """
    import_assay_wizard.assay_selection_wizard.assay_creation_rb.setChecked(True)
    yield import_assay_wizard.assay_selection_wizard


def test_no_assay_defined(assay_selection_page, mocker):
    """
    Test assay selection is not validated if no variable defined

    Args:
        import_assay_wizard: fixture to create a AssayImportWizard
        mocker: pytest-mock to accept QMessageBox warning
    """

    # mock assay selection not validated (no variable defined)
    msg_box_ok_mock = mocker.patch.object(
        QMessageBox, "warning", return_value=QMessageBox.Ok
    )
    assert not assay_selection_page.validatePage()
    msg_box_ok_mock.assert_called_once()


params = [
    (AssayDomainType.DEPTH, AssayDataExtent.EXTENDED),
    (AssayDomainType.TIME, AssayDataExtent.EXTENDED),
    (AssayDomainType.DEPTH, AssayDataExtent.DISCRETE),
    (AssayDomainType.TIME, AssayDataExtent.DISCRETE),
]


@pytest.fixture(params=params)
def assay_creation_selection_page(request, test_connection, assay_selection_page):
    """
    Fixture to get a AssayImportWizard assay selection page with a discrete assay
    Args:
        assay_selection_page:
    """
    assay_name = "assay"
    if request.param[1] == AssayDataExtent.DISCRETE:
        (definition, table) = create_assay_discrete_definition_object(
            assay_name,
            test_connection.get_assay_iface().default_assay_schema(),
            request.param[0],
            AssaySeriesType.NUMERICAL,
        )
    else:
        (definition, table) = create_assay_extended_definition_object(
            assay_name,
            test_connection.get_assay_iface().default_assay_schema(),
            request.param[0],
            AssaySeriesType.NUMERICAL,
        )

    assay_selection_page.assay_definition_widget.set_assay_definition(definition)
    yield assay_selection_page


def test_assay_variable_defined(assay_creation_selection_page):
    """
    Test assay selection is not validated if no variable defined

    Args:
        assay_creation_selection_page: fixture to get a AssayImportWizard selection page
    """

    # If no assay define in database, assay creation is checked
    assert assay_creation_selection_page.assay_creation_rb.isChecked()

    assert assay_creation_selection_page.validatePage()


# ---------------- TESTS FOR IMPORT PAGE ----------------------------------
@pytest.fixture()
def import_wizard_with_valid_data(
    import_assay_wizard, assay_creation_selection_page, tmp_path
):
    """
    Fixture to create a import wizard page with valid data

    Args:
        import_assay_wizard: fixture to create a AssayImportWizard
        assay_creation_selection_page: fixture to get a AssayImportWizard assay definition page
        tmp_path: pytest temporary path for collar csv creation
    """
    assert assay_creation_selection_page.validatePage()

    assay_definition = import_assay_wizard.assay_import_wizard.get_assay_definition()

    import_wizard_page = import_assay_wizard.assay_import_wizard
    import_wizard_page.initializePage()

    # Simulate add of y column
    import_wizard_page.dataset_edit.add_column(column="y", mapping="y")

    if assay_definition.data_extent == AssayDataExtent.DISCRETE:
        columns = [import_wizard_page._HOLE_ID, import_wizard_page._X_COL, "y"]
        if assay_definition.domain == AssayDomainType.DEPTH:
            values = [["assay", "0", "0"], ["assay", "1", "1"]]
        else:
            values = [["assay", "2020-02-01", "0"], ["assay", "2020-02-02", "1"]]
    else:
        columns = [
            import_wizard_page._HOLE_ID,
            import_wizard_page._X_COL,
            import_wizard_page._X_END_COL,
            "y",
        ]
        if assay_definition.domain == AssayDomainType.DEPTH:
            values = [["assay", "0", "1", "0"], ["assay", "1", "2", "1"]]
        else:
            values = [
                ["assay", "2020-02-01", "2020-02-02", "0"],
                ["assay", "2020-02-02", "2020-02-03", "1"],
            ]

    csv_file = create_file(tmp_path, "assay.csv", columns, values, ";")

    import_wizard_page.dataset_edit.set_delimiter(";")
    import_wizard_page.dataset_edit.filename_edit.lineEdit().setText(str(csv_file))
    import_wizard_page.dataset_edit._update_table_and_fields()

    yield import_assay_wizard


# ---------------- TESTS FOR DATABASE DEFINITION PAGE ----------------------------------
@pytest.fixture()
def assay_definition_database_page(import_wizard_with_valid_data):
    """
    Fixture to get a AssayImportWizard assay selection page

    Args:
        import_wizard_with_valid_data: fixture to create a AssayImportWizard with valid import data
    """
    import_wizard_with_valid_data.assay_import_wizard.validatePage()
    assay_database_definition_wizard = (
        import_wizard_with_valid_data.assay_database_definition_wizard
    )
    assay_database_definition_wizard.initializePage()
    yield assay_database_definition_wizard


def test_assay_invalid_table_definition(assay_definition_database_page, mocker):
    """
    Test assay database definition is not validated if invalid table definition

    Args:
        assay_definition_database_page: fixture to get a AssayImportWizard database definition page
        mocker: pytest-mock to accept QMessageBox warning
    """

    # mock assay database definition not validated (no table name defined)
    assay_definition_database_page.assay_database_definition_widget.table_cb.setCurrentText(
        ""
    )
    msg_box_ok_mock = mocker.patch.object(
        QMessageBox, "warning", return_value=QMessageBox.Ok
    )
    assert not assay_definition_database_page.validatePage()
    msg_box_ok_mock.assert_called_once()

    assay_definition_database_page.assay_database_definition_widget.table_cb.setCurrentText(
        "table"
    )
    assay_definition_database_page.assay_database_definition_widget.hole_id_cb.setCurrentText(
        "invalid col"
    )

    msg_box_ok_mock = mocker.patch.object(
        QMessageBox, "warning", return_value=QMessageBox.Ok
    )
    assert not assay_definition_database_page.validatePage()
    msg_box_ok_mock.assert_called_once()


@pytest.fixture()
def valid_assay_definition_database_page(assay_definition_database_page):
    """
    Fixture to get a AssayImportWizard database definition page with valid value

    Args:
        assay_definition_database_page: fixture to get a AssayImportWizard database definition page
    """

    # Define table name
    assay_definition_database_page.assay_database_definition_widget.table_cb.setCurrentText(
        "table"
    )
    yield assay_definition_database_page


def test_assay_database_definition_valid(valid_assay_definition_database_page):
    """
    Test assay selection is not validated if no variable defined

    Args:
        valid_assay_definition_database_page: fixture to get a AssayImportWizard database definition page
    """
    assert valid_assay_definition_database_page.validatePage()


# ---------------- TESTS FOR CONFIRMATION PAGE ----------------------------------
def test_assay_import_valid(import_wizard_with_valid_data):
    """
    Test assay import with a valid wizard

    Args:
        import_wizard_with_valid_data: fixture to get a AssayImportWizard import data page
    """
    import_wizard_with_valid_data.assay_import_wizard.validatePage()
    import_wizard_with_valid_data.assay_database_definition_wizard.initializePage()
    import_wizard_with_valid_data.assay_database_definition_wizard.validatePage()
    import_wizard_with_valid_data.confirmation_page.initializePage()
    assert import_wizard_with_valid_data.confirmation_page.validatePage()
