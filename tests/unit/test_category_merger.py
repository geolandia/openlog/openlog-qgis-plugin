import pandas as pd
import pytest

from openlog.core.category_merger import merge_contiguous_items

df_in = pd.DataFrame(
    {
        "from": [0, 10, 20, 30, 40, 50, 60],
        "to": [10, 20, 30, 40, 50, 60, 70],
        "code1": ["a", "b", "b", "b", "b", "c", "d"],
        "code2": ["e", "f", "f", "g", "g", "g", "h"],
    }
)


expected_1 = pd.DataFrame(
    {"from": [0, 10, 50, 60], "to": [10, 50, 60, 70], "code1": ["a", "b", "c", "d"]}
)

expected_2 = pd.DataFrame(
    {"from": [0, 10, 30, 60], "to": [10, 30, 60, 70], "code2": ["e", "f", "g", "h"]}
)

expected_3 = pd.DataFrame(
    {
        "from": [0, 10, 30, 50, 60],
        "to": [10, 30, 50, 60, 70],
        "code1": ["a", "b", "b", "c", "d"],
        "code2": ["e", "f", "g", "g", "h"],
    }
)


@pytest.mark.parametrize(
    "df, y_column, expected",
    [
        (df_in, ["code1"], expected_1),
        (df_in, ["code2"], expected_2),
        (df_in, ["code1", "code2"], expected_3),
    ],
)
def test_merger(df: pd.DataFrame, y_column: [str], expected: pd.DataFrame):
    x = df[["from", "to"]]
    y = df[y_column]
    new_x, new_y = merge_contiguous_items(x, y)
    result = pd.concat([pd.DataFrame(new_x), pd.DataFrame(new_y)], axis=1)
    result.columns = ["from", "to"] + y_column

    pd.testing.assert_frame_equal(result, expected, check_dtype=False)
