import pandas as pd
import pytest

from openlog.core.assay_interpolation import (
    ExtendedAssayColumn,
    ExtendedAssayInterpolation,
    GapDataResolution,
    OverlapDataResolution,
)


def _compare_dataframe(new_df: pd.DataFrame, expected: pd.DataFrame) -> None:
    pd.testing.assert_frame_equal(
        new_df.reset_index(drop=True),
        expected.reset_index(drop=True),
        check_column_type=False,
        check_dtype=False,
    )


EMPTY_DF = pd.DataFrame(
    {
        "hole_id": [],
        "from": [],
        "to": [],
        "lith_code": [],
        "float_values": [],
        "update_status": [],
    }
)

import_params = "gap_resolution, overlap_resolution, expected"


@pytest.fixture
def lith_col():
    return ExtendedAssayColumn(
        hole_id_col="hole_id",
        from_col="from",
        to_col="to",
        val_cols=["lith_code", "float_values"],
    )


@pytest.fixture()
def gap_df():
    return pd.DataFrame(
        {
            "hole_id": ["B", "B", "B"],
            "from": [0.0, 100.0, 300.0],
            "to": [100.0, 200.0, 400.0],
            "lith_code": ["up", "middle", "bottom"],
            "float_values": [1.0, 2.0, 3.0],
        }
    )


interpolation_gap_values = [
    (GapDataResolution.REJECT, OverlapDataResolution.REJECT, EMPTY_DF),
    (
        GapDataResolution.FORWARD_EXPANSION,
        OverlapDataResolution.REJECT,
        pd.DataFrame(
            {
                "hole_id": ["B", "B", "B", "B"],
                "from": [0.0, 100.0, 200.0, 300.0],
                "to": [100.0, 200.0, 300.0, 400.0],
                "lith_code": ["up", "middle", "bottom", "bottom"],
                "float_values": [1.0, 2.0, 3.0, 3.0],
                "update_status": [None, None, "expansion", None],
            }
        ),
    ),
    (
        GapDataResolution.BACKWARD_EXPANSION,
        OverlapDataResolution.REJECT,
        pd.DataFrame(
            {
                "hole_id": ["B", "B", "B", "B"],
                "from": [0.0, 100.0, 200.0, 300.0],
                "to": [100.0, 200.0, 300.0, 400.0],
                "lith_code": ["up", "middle", "middle", "bottom"],
                "float_values": [1.0, 2.0, 2.0, 3.0],
                "update_status": [None, None, "expansion", None],
            }
        ),
    ),
    (
        GapDataResolution.NEAREST_NEIGHBOR,
        OverlapDataResolution.REJECT,
        pd.DataFrame(
            {
                "hole_id": ["B", "B", "B", "B", "B"],
                "from": [0.0, 100.0, 200.0, 250.0, 300.0],
                "to": [100.0, 200.0, 250.0, 300.0, 400.0],
                "lith_code": ["up", "middle", "middle", "bottom", "bottom"],
                "float_values": [1.0, 2.0, 2.0, 3.0, 3.0],
                "update_status": [None, None, "expansion", "expansion", None],
            }
        ),
    ),
]


@pytest.mark.parametrize(
    import_params,
    interpolation_gap_values,
)
def test_interpolation_gap(
    lith_col, gap_df, gap_resolution, overlap_resolution, expected
):
    interp = ExtendedAssayInterpolation(
        gap_resolution=gap_resolution,
        overlap_resolution=overlap_resolution,
        columns=lith_col,
    )
    new_df = interp.interpolated_dataframe(gap_df)
    _compare_dataframe(new_df, expected)


@pytest.fixture()
def overlap_df():
    return pd.DataFrame(
        {
            "hole_id": ["C", "C", "C"],
            "from": [0.0, 50.0, 150.0],
            "to": [100.0, 150, 200.0],
            "lith_code": ["up", "middle", "bottom"],
            "float_values": [1.0, 2.0, 3.0],
        }
    )


interpolation_overlap_values = [
    (GapDataResolution.REJECT, OverlapDataResolution.REJECT, EMPTY_DF),
    (
        GapDataResolution.REJECT,
        OverlapDataResolution.FORWARD_EXPANSION,
        pd.DataFrame(
            {
                "hole_id": ["C", "C", "C"],
                "from": [0.0, 50.0, 150.0],
                "to": [50.0, 150.0, 200.0],
                "lith_code": ["up", "middle", "bottom"],
                "float_values": [1.0, 2.0, 3.0],
                "update_status": ["expansion", None, None],
            }
        ),
    ),
    (
        GapDataResolution.REJECT,
        OverlapDataResolution.BACKWARD_EXPANSION,
        pd.DataFrame(
            {
                "hole_id": ["C", "C", "C"],
                "from": [0.0, 100.0, 150.0],
                "to": [100.0, 150.0, 200.0],
                "lith_code": ["up", "middle", "bottom"],
                "float_values": [1.0, 2.0, 3.0],
                "update_status": [None, "expansion", None],
            }
        ),
    ),
    (
        GapDataResolution.REJECT,
        OverlapDataResolution.NEAREST_NEIGHBOR,
        pd.DataFrame(
            {
                "hole_id": ["C", "C", "C"],
                "from": [0.0, 75.0, 150.0],
                "to": [75.0, 150.0, 200.0],
                "lith_code": ["up", "middle", "bottom"],
                "float_values": [1.0, 2.0, 3.0],
                "update_status": ["expansion", "expansion", None],
            }
        ),
    ),
]


@pytest.mark.parametrize(
    import_params,
    interpolation_overlap_values,
)
def test_interpolation_overlap(
    lith_col, overlap_df, gap_resolution, overlap_resolution, expected
):
    interp = ExtendedAssayInterpolation(
        gap_resolution=gap_resolution,
        overlap_resolution=overlap_resolution,
        columns=lith_col,
    )
    new_df = interp.interpolated_dataframe(overlap_df)
    _compare_dataframe(new_df, expected)


@pytest.fixture()
def overlap_and_gap_df():
    return pd.DataFrame(
        {
            "hole_id": ["B", "B", "B", "C", "C", "C"],
            "from": [0.0, 100.0, 300.0, 0.0, 50.0, 150.0],
            "to": [100.0, 200.0, 400.0, 100.0, 150, 200.0],
            "lith_code": ["up", "middle", "bottom", "up", "middle", "bottom"],
            "float_values": [1.0, 2.0, 3.0, 1.0, 2.0, 3.0],
        }
    )


interpolation_mixed_values = [
    (GapDataResolution.REJECT, OverlapDataResolution.REJECT, EMPTY_DF),
    (
        GapDataResolution.REJECT,
        OverlapDataResolution.FORWARD_EXPANSION,
        pd.DataFrame(
            {
                "hole_id": ["C", "C", "C"],
                "from": [0.0, 50.0, 150.0],
                "to": [50.0, 150.0, 200.0],
                "lith_code": ["up", "middle", "bottom"],
                "float_values": [1.0, 2.0, 3.0],
                "update_status": ["expansion", None, None],
            }
        ),
    ),
    (
        GapDataResolution.REJECT,
        OverlapDataResolution.BACKWARD_EXPANSION,
        pd.DataFrame(
            {
                "hole_id": ["C", "C", "C"],
                "from": [0.0, 100.0, 150.0],
                "to": [100.0, 150.0, 200.0],
                "lith_code": ["up", "middle", "bottom"],
                "float_values": [1.0, 2.0, 3.0],
                "update_status": [None, "expansion", None],
            }
        ),
    ),
    (
        GapDataResolution.REJECT,
        OverlapDataResolution.NEAREST_NEIGHBOR,
        pd.DataFrame(
            {
                "hole_id": ["C", "C", "C"],
                "from": [0.0, 75.0, 150.0],
                "to": [75.0, 150.0, 200.0],
                "lith_code": ["up", "middle", "bottom"],
                "float_values": [1.0, 2.0, 3.0],
                "update_status": ["expansion", "expansion", None],
            }
        ),
    ),
    (
        GapDataResolution.FORWARD_EXPANSION,
        OverlapDataResolution.REJECT,
        pd.DataFrame(
            {
                "hole_id": ["B", "B", "B", "B"],
                "from": [0.0, 100.0, 200, 300.0],
                "to": [100.0, 200.0, 300, 400.0],
                "lith_code": ["up", "middle", "bottom", "bottom"],
                "float_values": [1.0, 2.0, 3.0, 3.0],
                "update_status": [None, None, "expansion", None],
            }
        ),
    ),
    (
        GapDataResolution.BACKWARD_EXPANSION,
        OverlapDataResolution.REJECT,
        pd.DataFrame(
            {
                "hole_id": ["B", "B", "B", "B"],
                "from": [0.0, 100.0, 200, 300.0],
                "to": [100.0, 200.0, 300, 400.0],
                "lith_code": ["up", "middle", "middle", "bottom"],
                "float_values": [1.0, 2.0, 2.0, 3.0],
                "update_status": [None, None, "expansion", None],
            }
        ),
    ),
    (
        GapDataResolution.NEAREST_NEIGHBOR,
        OverlapDataResolution.REJECT,
        pd.DataFrame(
            {
                "hole_id": ["B", "B", "B", "B", "B"],
                "from": [0.0, 100.0, 200.0, 250.0, 300.0],
                "to": [100.0, 200.0, 250.0, 300.0, 400.0],
                "lith_code": ["up", "middle", "middle", "bottom", "bottom"],
                "float_values": [1.0, 2.0, 2.0, 3.0, 3.0],
                "update_status": [None, None, "expansion", "expansion", None],
            }
        ),
    ),
    (
        GapDataResolution.NEAREST_NEIGHBOR,
        OverlapDataResolution.NEAREST_NEIGHBOR,
        pd.DataFrame(
            {
                "hole_id": ["B", "B", "B", "B", "B", "C", "C", "C"],
                "from": [0.0, 100.0, 200.0, 250.0, 300.0, 0.0, 75.0, 150.0],
                "to": [100.0, 200.0, 250.0, 300.0, 400.0, 75.0, 150.0, 200.0],
                "lith_code": [
                    "up",
                    "middle",
                    "middle",
                    "bottom",
                    "bottom",
                    "up",
                    "middle",
                    "bottom",
                ],
                "float_values": [1.0, 2.0, 2.0, 3.0, 3.0, 1.0, 2.0, 3.0],
                "update_status": [
                    None,
                    None,
                    "expansion",
                    "expansion",
                    None,
                    "expansion",
                    "expansion",
                    None,
                ],
            }
        ),
    ),
    (
        GapDataResolution.BACKWARD_EXPANSION,
        OverlapDataResolution.NEAREST_NEIGHBOR,
        pd.DataFrame(
            {
                "hole_id": ["B", "B", "B", "B", "C", "C", "C"],
                "from": [0.0, 100.0, 200, 300.0, 0.0, 75.0, 150.0],
                "to": [100.0, 200.0, 300, 400.0, 75.0, 150.0, 200.0],
                "lith_code": [
                    "up",
                    "middle",
                    "middle",
                    "bottom",
                    "up",
                    "middle",
                    "bottom",
                ],
                "float_values": [1.0, 2.0, 2.0, 3.0, 1.0, 2.0, 3.0],
                "update_status": [
                    None,
                    None,
                    "expansion",
                    None,
                    "expansion",
                    "expansion",
                    None,
                ],
            }
        ),
    ),
]


@pytest.mark.parametrize(
    import_params,
    interpolation_mixed_values,
)
def test_interpolation_mixed_values(
    lith_col, overlap_and_gap_df, gap_resolution, overlap_resolution, expected
):
    interp = ExtendedAssayInterpolation(
        gap_resolution=gap_resolution,
        overlap_resolution=overlap_resolution,
        columns=lith_col,
    )
    new_df = interp.interpolated_dataframe(overlap_and_gap_df)
    _compare_dataframe(new_df, expected)


def test_invalid_input_data(lith_col):
    interp = ExtendedAssayInterpolation(
        gap_resolution=GapDataResolution.REJECT,
        overlap_resolution=OverlapDataResolution.REJECT,
        columns=lith_col,
    )

    df = pd.DataFrame(
        {
            "hole_id": ["B", "B", "B"],
            "from": ["aa", "aa", "aa"],
            "to": ["aa", "aa", "aa"],
            "lith_code": ["up", "middle", "bottom"],
            "float_values": [1.0, 2.0, 3.0],
        }
    )

    with pytest.raises(ExtendedAssayInterpolation.InvalidColumnData):
        interp.interpolated_dataframe(df)

    df = pd.DataFrame(
        {
            "hole_id": ["B", "B", "B"],
            "from": [100.0, 200.0, 300.0],
            "to": ["aa", "aa", "aa"],
            "lith_code": ["up", "middle", "bottom"],
            "float_values": [1.0, 2.0, 3.0],
        }
    )

    with pytest.raises(ExtendedAssayInterpolation.InvalidColumnData):
        interp.interpolated_dataframe(df)

    df = pd.DataFrame(
        {
            "hole_id": ["B", "B", "B"],
            "from": ["aa", "aa", "aa"],
            "to": [100.0, 200.0, 300.0],
            "lith_code": ["up", "middle", "bottom"],
            "float_values": [1.0, 2.0, 3.0],
        }
    )

    with pytest.raises(ExtendedAssayInterpolation.InvalidColumnData):
        interp.interpolated_dataframe(df)
